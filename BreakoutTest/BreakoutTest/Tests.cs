﻿using System.Drawing.Imaging;
using BreakoutGame;
using BreakoutGame.GameObjects;
using BreakoutTest.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using NUnit.Framework;

namespace BreakoutTest
{
    /// <summary>
    /// Test class
    /// </summary>
    public class Tests
    {
        /// <summary>
        /// The content
        /// </summary>
        private ContentManager content;

        [TestFixtureSetUp]
        public void SetUpTest()
        {
            AssemblyUtilities.Copycontent();

            var gd = new MockedGraphicsDeviceService(DeviceType.Hardware, GraphicsProfile.HiDef);
            gd.CreateDevice();
            content = new ContentManager(gd.ServiceProvider) {RootDirectory = "Content"};
        }

        [SetUp]
        public void SetUp()
        {
            AssemblyUtilities.SetEntryAssembly();
        }

        [Test]
        public void LoadContentTest()
        {

            var resource = content.Load<Texture2D>("Background\\intro");
        }

        /// <summary>
        /// Paddle and the ball collision test.
        /// </summary>
        [Test]
        public void PaddleAndBallCollision()
        {
            Ball ball = new Ball(content, @"GameObject\ball1", 0, 0, MyGame.BallSize);
            Player player = new Player(content, @"GameObject\player1", 0, 0, MyGame.PlayerWidth, MyGame.PlayerHeight);

            bool collTest = Collision.IsTouching(ball.GetRectangle, player.GetRectangle);

            Assert.IsTrue(collTest);
        }


        /// <summary>
        /// Ball and the brick collision test.
        /// </summary>
        [Test]
        public void BallAndBrickCollision()
        {
            Ball ball = new Ball(content, @"GameObject\ball1", 0, 0, MyGame.BallSize);
            Brick brick = new Brick(content, MyGame.GameBrickBlue, 0, 0, MyGame.BrickWidth, MyGame.BrickHeight);

            bool collTest = Collision.IsTouching(ball.GetRectangle, brick.GetRectangle);

            Assert.IsTrue(collTest);
        }

        /// <summary>
        /// Ball movement test.
        /// </summary>
        [Test]
        public void BallMovement()
        {
            MyGame myGame = new MyGame();

            myGame.ball = new Ball(content,@"GameObject\ball1",0,0,MyGame.BallSize);

            Rectangle ballPosInstance = myGame.ball.GetRectangle;

            myGame.BallMoving();

            Assert.AreNotEqual(ballPosInstance, myGame.ball.GetRectangle);

        }
    }
}
