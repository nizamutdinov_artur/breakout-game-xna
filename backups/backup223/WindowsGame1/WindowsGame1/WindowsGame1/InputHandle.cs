﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace WindowsGame1
    {
    public static class InputHandle
        {
        private static bool _aPressed = false;
        private static bool _dPressed = false;
        private static bool _leftPressed = false;
        private static bool _rightPressed = false;

        private static bool _enterPressed = false;
        private static bool _spacePressed = false;

        #region Public

        public static bool APressed
            {
            get { return _aPressed; }
            private set { _aPressed = value; }
            }

        public static bool DPressed
            {
            get { return _dPressed; }
            private set { _dPressed = value; }
            }

        public static bool LeftPressed
            {
            get { return _leftPressed; }
            private set { _leftPressed = value; }
            }

        public static bool RightPressed
            {
            get { return _rightPressed; }
            private set { _rightPressed = value; }
            }

        public static bool EnterPressed
            {
            get { return _enterPressed; }
            private set { _enterPressed = value; }
            }

        public static bool SpacePressed
            {
            get { return _spacePressed; }
            private set { _spacePressed = value; }
            }
        #endregion

        public static void CheckState( )
            {
            _leftPressed = Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Left );
            _rightPressed = Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Right );
            _aPressed = Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.A );
            _dPressed = Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.D );

            _enterPressed = Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Enter );
            _spacePressed = Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Space );

            }
        }
    }