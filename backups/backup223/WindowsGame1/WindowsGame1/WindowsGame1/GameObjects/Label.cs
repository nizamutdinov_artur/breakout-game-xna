﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{
    internal class Label
    {
        private SpriteFont _font1;
        private Vector2 _fontPos;


        public string Content { get; set; }

        /// <summary>
        /// Label ctor
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="text">Label text</param>
        /// <param name="instanceOfGame">Instance of base class</param>
        public Label(ContentManager contentManager, int x, int y, Game1 instanceOfGame, string text)
        {
            Content = text;

            _font1 = contentManager.Load<SpriteFont>("MyFont");

            _fontPos = new Vector2(x, y);
        }

        public Label(ContentManager contentManager, int x, int y, Game1 instanceOfGame)
        {
            Content = "Label";

            _font1 = contentManager.Load<SpriteFont>("MyFont");

            _fontPos = new Vector2(x, y);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_font1, Content, _fontPos, Color.White);
        }
    }
}