﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{
    internal class Player
    {
        private Rectangle _drawRectangle;
        private int _moveSpeed = 12;

        private Texture2D _sprite;


        public Player(ContentManager contentManager, string spriteName, int x, int y, int width, int height)
        {
            // load sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _drawRectangle = new Rectangle(x - _sprite.Width/2, y - _sprite.Height/2, width, height);
        }

        public int MoveSpeed
        {
            get { return _moveSpeed; }
            set { _moveSpeed = value; }
        }

        public int X
        {
            get { return _drawRectangle.X; }
            set { _drawRectangle.X = value; }
        }

        public int Y
        {
            get { return _drawRectangle.Y; }
            set { _drawRectangle.Y = value; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _drawRectangle, Color.White);
        }

    }
}