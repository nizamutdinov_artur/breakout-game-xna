﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{
    internal class Ball
    {
        private Rectangle _drawRectangle;
        private Texture2D _sprite;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contentManager"></param>
        /// <param name="spriteName"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        public Ball(ContentManager contentManager, string spriteName, int x, int y, int width, int height)
        {
            // load sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _drawRectangle = new Rectangle(x - _sprite.Width/2, y - _sprite.Height/2, width, height);
        }

        public int X
        {
            get { return _drawRectangle.X; }
            set { _drawRectangle.X = value; }
        }

        public int Y
        {
            get { return _drawRectangle.Y; }
            set { _drawRectangle.Y = value; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _drawRectangle, Color.White);
        }

    }
}