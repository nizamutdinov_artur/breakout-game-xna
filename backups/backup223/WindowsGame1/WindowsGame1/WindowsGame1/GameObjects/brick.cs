﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{

    public class Brick
    {

        private Rectangle _drawRectangle;
        private Texture2D _sprite;
        private string _spriteName;


        /// <summary>
        /// The constructor takes a coordinates
        /// and some options to place the brick successfully
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="spriteName">texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">brick width</param>
        /// <param name="height">brick heigth</param>
        public Brick(ContentManager contentManager, string spriteName, int x, int y, int width, int height)
        {
            _spriteName = spriteName;

            // load _sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _drawRectangle = new Rectangle(x - _sprite.Width/2, y - _sprite.Height/2, width, height);

        }


        public int X
        {
            get { return _drawRectangle.X; }
            set { _drawRectangle.X = value; }
        }

        public int Y
        {
            get { return _drawRectangle.Y; }
            set { _drawRectangle.Y = value; }
        }

        public string SpriteName
        {
            get { return _spriteName; }
            set { _spriteName = value; }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _drawRectangle, Color.White);

        }

    }
}
