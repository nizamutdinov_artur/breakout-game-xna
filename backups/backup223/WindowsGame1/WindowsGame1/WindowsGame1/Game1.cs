﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using WindowsGame1.GameObjects;
using Microsoft.Win32;

namespace WindowsGame1
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private const string GAME_NAME = "BREAKOUT";
        private const int GAME_FPS = 60;

        private const string GAME_BALL_SPRITE = "ball1";
        private const string GAME_PLAYER_SPRITE = "player1";

        ///private const string GAME
        private const int BRICK_ROW = 13;
        private const int BRICK_COLUMN = 6;

        public const int WINDOW_WIDTH = 800;
        public const int WINDOW_HEIGHT = 600;

        ///Game object sizes
        public const int PLAYER_WIDTH = 100;
        public const int PLAYER_HEIGHT = 10;
        public const int BRICK_WIDTH = 60;
        public const int BRICK_HEIGHT = 15;
        public const int BALL_SIZE = 15;

        private bool AI = false;
        private int fps;

        private int Game_Lives = 3;
        private int Game_Score = 0;
        private string Game_State = "Idle";

        private double ballSpeedX = 1;
        private double ballSpeedY = 1;
        private int prevPosX;
        private int prevPosY;
        private int ballSpeedModifer = 8;

        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        /// <summary>
        /// Base game constructor
        /// </summary>
        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);

            Content.RootDirectory = "Content";
            this.Window.Title = GAME_NAME;

            // Mouse visible
            this.IsMouseVisible = true;

            // Window resolution
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;

            // Set fps to GAME_FPS constant
            this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f/(float)GAME_FPS);
        }


        protected override void Initialize()
        {
            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // Game tools

            // Game objects
            player = new Player(Content, GAME_PLAYER_SPRITE, WINDOW_WIDTH, WINDOW_HEIGHT + 60, PLAYER_WIDTH, PLAYER_HEIGHT);
            ball = new Ball(Content, GAME_BALL_SPRITE, WINDOW_WIDTH/2, WINDOW_HEIGHT/2, BALL_SIZE, BALL_SIZE);
            InitBricks();

            // Labels
            livesLabel = new Label(Content, WINDOW_WIDTH - 100, 10, this);
            scoreLabel = new Label(Content, 10, 10, this);
            enterGameLabel = new Label(Content, WINDOW_HEIGHT/2, WINDOW_WIDTH/2 - 100, this, "Press enter to game");
            fpsLabel = new Label(Content, 300, 10, this);

            

        }

        /// <summary>
        /// The method of the game loop.
        /// He runs every frame.
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update(GameTime gameTime)
        {
            Helper();

            InputHandle.CheckState(); // Refresh keyboard state

            fps = Fps.CheckFps( gameTime );

            livesLabel.Content = "Lives: " + Game_Lives;
            scoreLabel.Content = "Score: " + Game_Score;
            fpsLabel.Content = "FPS: " + fps;

            if (Game_State == "Game")
            {
                PlayerMoving();
                BallMoving();

                PlayerCollisionTest();
                BrickCollisionTest();
            }
            else if (Game_State == "Idle")
            {
                ball.X = player.X + (PLAYER_WIDTH/2) - BALL_SIZE/2;
                ball.Y = player.Y - BALL_SIZE;
                if (InputHandle.EnterPressed == true)
                {
                    Game_State = "Game";
                }
                PlayerMoving();
            }

            base.Update(gameTime);
        }

        private void Helper()
        {
            if (InputHandle.SpacePressed)
            {
                AI = true;
            }

            if (AI && ball.Y < WINDOW_HEIGHT - 50)
            {
                player.X = ball.X - (PLAYER_WIDTH/2);
            }
        }


        /// <summary>
        /// Override a default XNA Draw method 
        /// </summary>
        /// <param name="gameTime">Gametime</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            if (Game_State == "Game")
            {
                player.Draw(spriteBatch);
                ball.Draw(spriteBatch);
                DrawBricks();

                DrawUi();
            }
            else if (Game_State == "Idle")
            {
                player.Draw(spriteBatch);
                ball.Draw(spriteBatch);
                DrawBricks();
                enterGameLabel.Draw(spriteBatch);

                DrawUi();
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Draws the bricks from the array List
        /// </summary>
        private void DrawBricks()
        {
            for (int i = 0; i < bricks.Count(); i++)
                if (bricks[i] != null)
                    bricks[i].Draw(spriteBatch);
        }

        private void DrawUi()
        {
            scoreLabel.Draw(spriteBatch);
            livesLabel.Draw(spriteBatch);
            fpsLabel.Draw(spriteBatch);
        }

        private void BrickCollisionTest()
        {
            for (int i = 0; i < bricks.Count(); i++)
            {
                if (bricks[i] != null &&
                    Collision.Test(bricks[i].X, bricks[i].Y, BRICK_WIDTH, BRICK_HEIGHT, ball.X, ball.Y, BALL_SIZE, BALL_SIZE))
                {
                    Beepenator.Beep("Brick");

                    // Find the angle between center of brick and a ball center.
                    int angle = (int) (Math.Atan2((bricks[i].Y + BRICK_HEIGHT/2) - (ball.Y + BALL_SIZE/2),
                        (bricks[i].X + BRICK_WIDTH/2) - (ball.X + BALL_SIZE/2))/Math.PI*180);

                    if (angle < 10 && angle > -10) // Angles between -10 and 10.
                        ballSpeedX = -Math.Abs(ballSpeedX);
                    else if ((angle < 180 && angle > 170) || (angle > -180 && angle < -170))
                        // Angles between 180 and -170
                        ballSpeedX = Math.Abs(ballSpeedX);
                    else if (angle > 10 && angle < 170)
                        ballSpeedY = -Math.Abs(ballSpeedY);
                    else if (angle < -10 && angle > -170)
                        ballSpeedY = Math.Abs(ballSpeedY);

                    switch (bricks[i].SpriteName)
                    {
                        case "":
                            break;
                        default:
                            Game_Score += 10;
                            break;
                    }

                    Game_Score += 100;
                    bricks[i] = null;
                    break;
                }
            }
        }

        private void PlayerCollisionTest()
        {
            if (Collision.Test(player.X, player.Y, PLAYER_WIDTH, PLAYER_HEIGHT, ball.X, ball.Y, BALL_SIZE, BALL_SIZE))
            {
                Beepenator.Beep("Player");
                double mod = ball.X - (player.X + PLAYER_WIDTH/2);
                ballSpeedX = (mod/Math.Abs(mod))*0.3 + mod/(PLAYER_WIDTH);
                ballSpeedY = -Math.Sqrt(2 - ballSpeedX*ballSpeedX);
            }

        }

        private void PlayerMoving()
        {
            if (InputHandle.DPressed && (player.X < WINDOW_WIDTH - PLAYER_WIDTH))
            {
                player.X += player.MoveSpeed;
            }

            if (InputHandle.APressed && (player.X > 0))
            {
                player.X -= player.MoveSpeed;
            }

            if (InputHandle.RightPressed && (player.X < WINDOW_WIDTH))
            {
                player.X += player.MoveSpeed;
            }

            if (InputHandle.LeftPressed && (player.X > 0))
            {
                player.X -= player.MoveSpeed;
            }
        }

        /// <summary>
        /// This method include ball moving
        /// and the Wall collision tester
        /// </summary>
        private void BallMoving()
        {
            prevPosX = ball.X;
            prevPosY = ball.Y;

            ball.X += (int) (ballSpeedX*ballSpeedModifer);
            ball.Y += (int) (ballSpeedY*ballSpeedModifer);

            // Left
            if (ball.X < 0)
            {
                ballSpeedX = Math.Abs(ballSpeedX);
                Beepenator.Beep("Wall");
            }
            // Right
            if (ball.X > WINDOW_WIDTH - BALL_SIZE)
            {
                ballSpeedX = -Math.Abs(ballSpeedX);
                Beepenator.Beep("Wall");
            }
            // Upper
            if (ball.Y < 0)
            {
                ballSpeedY = Math.Abs(ballSpeedY);
                Beepenator.Beep("Wall");
            }
            // Bottom
            if (ball.Y > WINDOW_HEIGHT - BALL_SIZE)
            {
                BallMissed();
            }

        }

        /// <summary>
        /// What happends if player miss the ball
        /// </summary>
        private void BallMissed()
        {

            Beepenator.Beep("BallMissed");

            if (Game_Lives > 1)
            {
                Game_Lives--;
                ball.X = WINDOW_WIDTH/2;
                ball.Y = WINDOW_HEIGHT/2;
                Game_State = "Idle";
            }
            else
            {
                Game_State = "Lose";

                if (System.Windows.Forms.MessageBox.Show("Вы проиграли", "Game", System.Windows.Forms.MessageBoxButtons.RetryCancel, System.Windows.Forms.MessageBoxIcon.Asterisk) == System.Windows.Forms.DialogResult.Retry)
                {

                    System.Windows.Forms.MessageBox.Show("Вы набрали " + Game_Score + " очков!", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);

                    Game_State = "Idle";
                    Game_Score = 0;
                    Game_Lives = 3;
                    player.X = (WINDOW_WIDTH/2) - (PLAYER_WIDTH/2);

                    bricks = new List<Brick>();

                    InitBricks();

                }
                else
                {
                    this.Exit();
                }
            }
        }




        private void InitBricks()
        {
            for (int i = 0; i < BRICK_COLUMN; i++)
                for (int j = 0; j < BRICK_ROW; j++)
                {
                    const int marginX = 250;
                    const int marginY = 120;

                    int distanceX = j*60;
                    int distanceY = i*15;
                    switch (i)
                    {
                        case 0:
                        {
                            bricks.Add(new Brick(Content, "brickRed", marginX + distanceX, marginY + distanceY,
                                BRICK_WIDTH,
                                BRICK_HEIGHT));
                            break;
                        }
                        case 1:
                        {
                        bricks.Add( new Brick( Content, "brickOrange1", marginX + distanceX, marginY + distanceY, BRICK_WIDTH,
                            BRICK_HEIGHT ) );
                            break;
                        }
                        case 2:
                        {
                        bricks.Add( new Brick( Content, "brickOrange2", marginX + distanceX, marginY + distanceY, BRICK_WIDTH,
                            BRICK_HEIGHT ) );
                            break;
                        }
                        case 3:
                        {
                        bricks.Add( new Brick( Content, "brickYellow", marginX + distanceX, marginY + distanceY, BRICK_WIDTH,
                            BRICK_HEIGHT ) );
                            break;
                        }
                        case 4:
                        {
                        bricks.Add( new Brick( Content, "brickGreen", marginX + distanceX, marginY + distanceY, BRICK_WIDTH,
                            BRICK_HEIGHT ) );
                            break;
                        }
                        case 5:
                        {
                        bricks.Add( new Brick( Content, "brickBlue", marginX + distanceX, marginY + distanceY, BRICK_WIDTH,
                            BRICK_HEIGHT ) );
                            break;
                        }
                    }
                }

        }


        private Player player;
        private Ball ball;

        private List<Brick> bricks = new List<Brick>();

        private Label scoreLabel;
        private Label livesLabel;
        private Label fpsLabel;

        private Label enterGameLabel;
    }
}
