﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WindowsGame1
{

    public static class MySound
    {
        public static bool Sound = true;
        /// <summary>
        /// This method have a parameters:
        /// </summary>
        /// <param name="param">Player, Brick, Wall, BallMissed</param>
        public static void Beep(string param)
        {
            if (Sound)
            {
                Thread myThread;

                switch (param)
                {
                    case "Player":
                    {
                        myThread = new Thread(new ThreadStart(PlayerBeep));
                        myThread.Start();
                        break;
                    }
                    case "Brick":
                    {
                        myThread = new Thread(new ThreadStart(BrickBeep));
                        myThread.Start();
                        break;
                    }

                    case "Wall":
                    {
                        myThread = new Thread(new ThreadStart(WallBeep));
                        myThread.Start();
                        break;
                    }
                    case "BallMissed":
                    {
                        myThread = new Thread(new ThreadStart(BallMissedBeep));
                        myThread.Start();
                        break;
                    }
                    default:
                        break;
                }
            }
        }

        private static void BrickBeep()
        {
            Console.Beep(1000, 50);
        }

        private static void PlayerBeep()
        {
            Console.Beep(500, 50);
        }

        private static void WallBeep()
        {
            Console.Beep(2000, 50);
        }

        private static void BallMissedBeep()
        {
            Console.Beep(5000, 500);
        }
    }
}