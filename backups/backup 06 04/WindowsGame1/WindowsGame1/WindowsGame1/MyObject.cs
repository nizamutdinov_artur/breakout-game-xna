﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;


namespace WindowsGame1
{
    /// <summary>
    /// Class include a basic rectangle 
    /// </summary>
    public class MyObject
    {
        public Rectangle _rectangle;
        public Texture2D _sprite;

        public float _opacity;

        public float _x;
        public float _y;

        public int _width;
        public int _heigth;


       /// <summary>
       /// Rectangular object with texture fill.
       /// </summary>
       /// <param name="contentManager">Instance of the content manager class.</param>
       /// <param name="spriteName">Sprite name of the object.</param>
       /// <param name="x">X coordinate of the object.</param>
        /// <param name="y">Y coordinate of the object.</param>
        /// <param name="width">Width of the object.</param>
        /// <param name="height">Heigth of the object.</param>
        /// <param name="opacity">Opacity of the object.</param>
        public MyObject(ContentManager contentManager, string spriteName, float x, float y, int width, int height, float opacity)
        {
            _x = x;
            _y = y;

            _width = width;
            _heigth = height;

            _opacity = opacity;

            // load _sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _rectangle = new Rectangle((int) _x, (int) _y, width, height);
        }
        /// <summary>
        /// Rectangular object with dynamic color filler
        /// </summary>
        /// <param name="x">X coordinate of the object</param>
        /// <param name="y">Y coordinate of the object</param>
        /// <param name="width">Width of the object</param>
        /// <param name="height">Heigth of the object</param>
        /// <param name="graphicsDevice">Instance of the graphic device class</param>
        /// <param name="color">Color of the object</param>
        /// <param name="opacity">Opacity of the object</param>
        public MyObject(float x, float y, int width, int height, GraphicsDevice graphicsDevice, Color color, float opacity)
        {
            _width = width;
            _heigth = height;

            _opacity = opacity;

            var data = new Color[width*height];

            for (int i = 0; i < data.Length; i++)
            {
                data[i] = color;
            }

            _sprite = new Texture2D(graphicsDevice, width, height);
            _sprite.SetData(data);

            _rectangle = new Rectangle((int) _x, (int) _y, width, height);

        }

        /// <summary>
        /// Get or Set X coordinate.
        /// </summary>
        public int X
        {
            get { return _rectangle.X; }
            set { _rectangle.X = value; }
        }

        /// <summary>
        /// Get or Set Y coordinate.
        /// </summary>
        public int Y
        {
            get { return _rectangle.Y; }
            set { _rectangle.Y = value; }
        }

        /// <summary>
        /// Width of the Object.
        /// </summary>
        public int Width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// Height of the Object.
        /// </summary>
        public int Heigth
        {
            get { return _heigth; }
            set { _heigth = value; }
        }

        /// <summary>
        /// Opacity of the Object.
        /// </summary>
        public float Opacity
        {
            get { return _opacity; }
            set { _opacity = value; }
        }

        /// <summary>
        /// Draw method.
        /// </summary>
        /// <param name="spriteBatch">Instance of SpriteBatch class.</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _rectangle, Color.White*_opacity);
        }

    }
}
