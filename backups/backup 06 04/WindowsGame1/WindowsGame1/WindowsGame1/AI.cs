﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using WindowsGame1.GameObjects;
using Microsoft.Xna.Framework;

namespace WindowsGame1
{
    /// <summary>
    /// TODO: Fix this bullshit
    /// </summary>
    public class AI
    {

    private Ball ball;
    private Brick[] bricks;


    // Ball props.
    private double ballSpeedX;
    private double ballSpeedY;
    private int ballSpeedMultiplier;

        public AI(Ball ball, List<Brick> bricks, double ballSpeedX, double ballSpeedY, int ballSpeedMultiplier)
        {
            this.ball = ball;           
            this.bricks = new Brick[bricks.Count];
            bricks.CopyTo(this.bricks);

            this.ballSpeedX = ballSpeedX;
            this.ballSpeedY = ballSpeedY;
            this.ballSpeedMultiplier = ballSpeedMultiplier;
        }

        public int CalculateFallPos()
        {
            while (ball.Y < Game1.WindowHeight - 50)
            {
                BrickCollisionTest();
                BallMoving();
            }
            return ball.X;
        }

        /// <summary>
        /// Checks collision bricks with the ball.
        /// </summary>
        private void BrickCollisionTest( )
            {
            for ( int i = 0; i < bricks.Count( ); i++ )
                {
                if ( bricks[ i ] != null &&
                    Collision.IsTouching( bricks[ i ].X, bricks[ i ].Y, Game1.BrickWidth, Game1.BrickHeight, ball.X, ball.Y, Game1.BallSize, Game1.BallSize ) )
                    {
                    // Find the angle between center of brick and a ball center.
                    int angle = ( int )( Math.Atan2( ( bricks[ i ].Y + Game1.BrickHeight / 2 ) - ( ball.Y + Game1.BallSize / 2 ),
                        ( bricks[ i ].X + Game1.BrickWidth / 2 ) - ( ball.X + Game1.BallSize / 2 ) ) / Math.PI * 180 );

                    if ( angle < 10 && angle > -10 ) // Angles between -10 and 10.
                        ballSpeedX = -Math.Abs( ballSpeedX );
                    else if ( ( angle < 180 && angle > 170 ) || ( angle > -180 && angle < -170 ) ) // Angles between 180 and -170 
                        ballSpeedX = Math.Abs( ballSpeedX );
                    else if ( angle > 10 && angle < 170 )
                        ballSpeedY = -Math.Abs( ballSpeedY );
                    else if ( angle < -10 && angle > -170 )
                        ballSpeedY = Math.Abs( ballSpeedY );

                    bricks[ i ] = null;
                    break;
                    }
                }
            }

        /// <summary>
        /// This method include ball moving
        /// and the Wall collision tester.
        /// </summary>
        private void BallMoving( )
            {
            ball.X += ( int )( ballSpeedX * ( ballSpeedMultiplier / 10 ) );
            ball.Y += ( int )( ballSpeedY * ( ballSpeedMultiplier / 10 ) );
            // Left
            if ( ball.X < 0 )
                {
                ballSpeedX = Math.Abs( ballSpeedX );
                }
            // Right
            if ( ball.X > Game1.WindowWidth - Game1.BallSize )
                {
                ballSpeedX = -Math.Abs( ballSpeedX );
                }
            // Upper
            if ( ball.Y < 0 )
                {
                ballSpeedY = Math.Abs( ballSpeedY );
                }

            }
    }
}
