﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace WindowsGame1
{
    /// <summary>
    /// TODO: First use
    /// </summary>
    public class AnimatedSprite : MyObject
    {
        private Texture2D _texture;


        private double _fps = 75;
        private readonly int _frameCount;
        private int _currentFrame;

        private float _elapsed;

        private readonly int _frameOffsetX;

        private readonly int _frameOffsetY;

        private bool _animating = true;

        public AnimatedSprite( int frameCount,int frameOffsetX, int frameOffsetY, ContentManager content, string spriteName, int x, int y, int width, int heigth,float opacity) : base(content, spriteName, x, y, width, heigth, opacity)
        {
            _frameOffsetX = frameOffsetX;
            _frameOffsetY = frameOffsetY;
            _frameCount = frameCount;
            _currentFrame = 0;
        }

        public void Update(GameTime gameTime)
        {
            _elapsed +=  gameTime.ElapsedGameTime.Milliseconds;
            if (_elapsed > (1000/_fps))
            {
                if (_animating)
                {
                    if (_currentFrame == _frameCount)
                        { _animating = false;}
                    _currentFrame++;

                }
                _elapsed = 0.0f;
            }

        }

        /// <summary>
        /// Get or Set (from 0 to _frameCount) Current Frame.
        /// </summary>
        public int Frame
        {
            get { return _currentFrame; }
            set { _currentFrame = (int) MathHelper.Clamp(value, 0, _frameCount); }
        }

        public double Fps
            {
            get { return _fps; }
            set { _fps = (float) Math.Max(value, 0f); }
        }

        public bool IsAnimating
        {
            get { return _animating; }
            set { _animating = value; }
        }

        public Rectangle GetSourceRectangle()
        {
            return new Rectangle(_frameOffsetX + (_width*_currentFrame), _frameOffsetY, _width, _heigth);
        }


        /// <summary>
        /// Draw method.
        /// </summary>
        /// <param name="spriteBatch">Instance of SpriteBatch class.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite,new Vector2(_x,_y), GetSourceRectangle(), Color.White*_opacity);
        }

    }
}
