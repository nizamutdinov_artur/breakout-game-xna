﻿using Microsoft.Xna.Framework.Content;

namespace WindowsGame1.GameObjects
{
    public class PowerPack : MyObject
    {

        public PowerPack(ContentManager contentManager, string spriteName, float x, float y, int width, int height, float opacity) : base(contentManager, spriteName, x, y, width, height, opacity)
        {

        }

    }
}
