﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{

    public class Brick
    {

        private Rectangle _drawRectangle;
        private readonly Texture2D _sprite;
        private readonly string _spriteName;


        /// <summary>
        /// For placing bricks carefully, constructor takes the position and some parameters.
        /// TODO: brick lives 
        /// TODO: Crashed field
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="spriteName">texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">brick width</param>
        /// <param name="height">brick heigth</param>
        public Brick(ContentManager contentManager, string spriteName, int x, int y, int width, int height)
        {
            _spriteName = spriteName;

            // load _sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _drawRectangle = new Rectangle(x, y, width, height);

        }

        /// <summary>
        /// Get or Set X coordinate
        /// </summary>
        public int X
        {
            get { return _drawRectangle.X; }
            set { _drawRectangle.X = value; }
        }

        /// <summary>
        /// Get or Set Y coordinate
        /// </summary>
        public int Y
        {
            get { return _drawRectangle.Y; }
            set { _drawRectangle.Y = value; }
        }

        /// <summary>
        /// Get name of sprite
        /// </summary>
        public string SpriteName
        {
            get { return _spriteName; }
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Instance of main SpriteBatch class</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _drawRectangle, Color.White);


        }

    }
}
