﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{
    internal class Player
    {
        private Rectangle _drawRectangle;
        private readonly Texture2D _sprite;
        private int _moveSpeed = 12;

        /// <summary>
        /// Player constructor
        /// </summary>
        /// <param name="contentManager">Instance of Content Manager</param>
        /// <param name="spriteName">Texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Player width</param>
        /// <param name="height">Player heigth</param>
        public Player(ContentManager contentManager, string spriteName, int x, int y, int width, int height)
        {
            // load sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _drawRectangle = new Rectangle(x, y, width, height);
        }

        /// <summary>
        /// Get or Set X coordinate
        /// </summary>
        public int X
        {
            get { return _drawRectangle.X; }
            set { _drawRectangle.X = value; }
        }

        /// <summary>
        /// Get or Set Y coordinate
        /// </summary>
        public int Y
        {
            get { return _drawRectangle.Y; }
            set { _drawRectangle.Y = value; }
        }

        /// <summary>
        /// Get or Set player MoveSpeed
        /// </summary>
        public int MoveSpeed
        {
            get { return _moveSpeed; }
            set { _moveSpeed = value; }
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Instance of main SpriteBatch class</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _drawRectangle, Color.White);
        }



    }
}