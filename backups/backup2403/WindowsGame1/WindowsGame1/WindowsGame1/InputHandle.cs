﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace WindowsGame1
    {
    public class InputHandle
        {
        private bool _aPressed = false;
        private bool _dPressed = false;
        private bool _leftPressed = false;
        private bool _rightPressed = false;

        private bool _enterPressed = false;
        private bool _spacePressed = false;

        #region Public

        public bool APressed
            {
            get { return _aPressed; }
            set { _aPressed = value; }
            }

        public bool DPressed
            {
            get { return _dPressed; }
            set { _dPressed = value; }
            }

        public bool LeftPressed
            {
            get { return _leftPressed; }
            set { _leftPressed = value; }
            }

        public bool RightPressed
            {
            get { return _rightPressed; }
            set { _rightPressed = value; }
            }

        public bool EnterPressed
            {
            get { return _enterPressed; }
            set {_enterPressed = value; }
            }

        public bool SpacePressed
            {
            get { return _spacePressed; }
            set { _spacePressed = value; }
            }
        #endregion

        public void CheckState( )
            {
            if ( Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Left ) ) { _leftPressed = true; }
            else { _leftPressed = false; }

            if ( Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Right ) ) { _rightPressed = true; }
            else { _rightPressed = false; }

            if ( Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.A ) ) { _aPressed = true; }
            else { _aPressed = false; }

            if ( Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.D ) ) { _dPressed = true; }
            else { _dPressed = false; }

            if ( Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Enter ) ) { _enterPressed = true; }
            else { _enterPressed = false; }

            if ( Keyboard.GetState( PlayerIndex.One ).IsKeyDown( Keys.Space ) ) { _spacePressed = true; }
            else { _spacePressed = false; }

            }
}
    }
