﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;

namespace WindowsGame1
    {

    public class Game1 : Microsoft.Xna.Framework.Game
        {
        private const string GAME_NAME = "BREAKOUT";
        private const int GAME_FRAMERATE = 125;

        private const int BRICK_ROW = 8;
        private const int BRICK_COLUMN = 6;

        public const int WINDOW_WIDTH = 800;
        public const int WINDOW_HEIGHT = 600;

        public const int PLAYER_WIDTH = 150;
        public const int PLAYER_HEIGHT = 15;
        public const int BRICK_WIDTH = 70;
        public const int BRICK_HEIGHT = 20;
        public const int BALL_SIZE = 20;

        public int total_Frames = 0;
        public float elapsed_Time = 0.0f;
        public int fps = 0;
        private bool AI = false;

        private int Game_Lives = 3;
        private int Game_Score = 0;
        private string Game_State = "Idle";

        private double ballSpeedX = 1;
        private double ballSpeedY = 1;
        private int prevPosX = 350;
        private int prevPosY = 250;
        private int ballSpeedModifer = 8;

        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        /// <summary>
        /// Base ctor
        /// </summary>
        public Game1( )
            {
            graphics = new GraphicsDeviceManager( this );

            Content.RootDirectory = "Content";
            this.Window.Title = GAME_NAME;

            // Mouse visible
            this.IsMouseVisible = true;
            // Window resolution
            graphics.PreferredBackBufferWidth = WINDOW_WIDTH;
            graphics.PreferredBackBufferHeight = WINDOW_HEIGHT;

            // Set fps to 75
            this.TargetElapsedTime = TimeSpan.FromSeconds( 1.0f / 75.0f );
            }


        protected override void Initialize( )
            {
            base.Initialize( );
            }

        protected override void LoadContent( )
            {
            spriteBatch = new SpriteBatch( GraphicsDevice );

            // Game utils
            beepenator = new Beepenator( );
            inputHandle = new InputHandle( );

            // Game objects
            player = new Player( Content, "player1", WINDOW_WIDTH, WINDOW_HEIGHT + 60, PLAYER_WIDTH, PLAYER_HEIGHT );
            ball = new Ball( Content, "ball1", WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2, BALL_SIZE, BALL_SIZE );

            // Labels
            livesLabel = new Label( Content, WINDOW_WIDTH - 100, 10, this );
            scoreLabel = new Label( Content, 10, 10, this );
            enterGameLabel = new Label( Content, WINDOW_HEIGHT / 2, WINDOW_WIDTH / 2 - 100, this, "Press enter to game" );
            fpsLabel = new Label( Content, 300, 10, this );

            InitBricks( );
            }

        /// <summary>
        /// Game loop method
        /// He calls every frame
        /// </summary>
        /// <param name="gameTime"></param>
        protected override void Update( GameTime gameTime )
            {
            Helper( );

            inputHandle.CheckState( );   // Refresh keyboard state

          fps = Fps.CheckFps( this, gameTime );

            livesLabel.Content = "Lives: " + Game_Lives;
            scoreLabel.Content = "Score: " + Game_Score;
            fpsLabel.Content = "FPS: " + fps;

            if ( Game_State == "Game" )
                {
                PlayerMoving( );
                BallMoving( );

                PlayerCollisionTest( );
                BrickCollisionTest( );
                }
            else if ( Game_State == "Idle" )
                {
                ball.X = player.X + ( PLAYER_WIDTH / 2 ) - BALL_SIZE / 2;
                ball.Y = player.Y - 20;
                if ( inputHandle.EnterPressed == true )
                    {
                    Game_State = "Game";
                    }
                PlayerMoving( );
                }

            base.Update( gameTime );
            }

        private void Helper( )
            {
            if ( inputHandle.SpacePressed )
                {
                AI = true;
                }

            if ( AI && ball.Y < WINDOW_HEIGHT - 50 )
                {
                player.X = ball.X - ( PLAYER_WIDTH / 2 );
                }
            }


        /// <summary>
        /// Override a default XNA Draw method 
        /// </summary>
        /// <param name="gameTime">Gametime</param>
        protected override void Draw( GameTime gameTime )
            {
            GraphicsDevice.Clear( Color.White );

            spriteBatch.Begin( );

            if ( Game_State == "Game" )
                {
                player.Draw( spriteBatch );
                ball.Draw( spriteBatch );
                DrawBricks( ); //Draw a bricks from List<Brick>    

                DrawUI( );
                }
            else if ( Game_State == "Idle" )
                {
                player.Draw( spriteBatch );
                ball.Draw( spriteBatch );
                DrawBricks( ); //Draw a bricks from List<Brick> 
                enterGameLabel.Draw( spriteBatch ); // Draw 

                DrawUI( );
                }

            spriteBatch.End( );

            base.Draw( gameTime );
            }

        private void DrawBricks( )
            {
            for ( int i = 0; i < bricks.Count( ); i++ )
                if ( bricks[ i ] != null )
                    bricks[ i ].Draw( spriteBatch );
            }

        /// <summary>
        /// TODO: FPS DRAW, ... ETC ETC ETC DRAWING UI AND (> _ < )
        /// </summary>
        private void DrawUI( )
            {
            scoreLabel.Draw( spriteBatch );
            livesLabel.Draw( spriteBatch );
            fpsLabel.Draw( spriteBatch );
            }

        private void BrickCollisionTest( )
            {
            int angle;

            for ( int i = 0; i < bricks.Count( ); i++ )
                {
                if ( bricks[ i ] != null &&
                    ( bricks[ i ].X - BALL_SIZE <= ball.X ) &&  // [bricks[i].X - BALLSIZE; +inf] 
                    ( bricks[ i ].X + BRICK_WIDTH >= ball.X ) &&     // [-inf; bricks[i].X + BRICK_WIDTH] 
                    ( bricks[ i ].Y - BALL_SIZE <= ball.Y ) &&    // [bricks[i] - BALLSIZE; +inf] 
                    ( bricks[ i ].Y + BRICK_HEIGHT >= ball.Y ) )      // [-inf; bricks[ i ].Y + BRICK_HEIGHT]
                    {

                    beepenator.Beep( "Brick" );

                    // Find the angle between center of brick and a ball center.
                    angle = ( int )( Math.Atan2( ( bricks[ i ].Y + BRICK_HEIGHT / 2 ) - ( ball.Y + BALL_SIZE / 2 ), ( bricks[ i ].X + BRICK_WIDTH / 2 ) - ( ball.X + BALL_SIZE / 2 ) ) / Math.PI * 180 );

                    if ( angle < 10 && angle > -10 )  // Angles between -10 and 10.
                        ballSpeedX = -Math.Abs( ballSpeedX );
                    else if ( ( angle < 180 && angle > 170 ) || ( angle > -180 && angle < -170 ) )      // Angles between 180 and -170
                        ballSpeedX = Math.Abs( ballSpeedX );
                    else if ( angle > 10 && angle < 170 )
                        ballSpeedY = -Math.Abs( ballSpeedY );
                    else if ( angle < -10 && angle > -170 )
                        ballSpeedY = Math.Abs( ballSpeedY );

                    bricks[ i ] = null;
                    Game_Score += 100;
                    break;
                    }
                }
            }

        /// <summary>
        /// ( Find left border ) && ( from -inf to right border )
        /// ( player.X <= ball.X ) && ( player.X >= ball.X - PLAYER_WIDTH )
        /// ------------------------------------------------------------------------
        /// From upper of border to +inf
        /// ( ball.Y > player.Y - PLAYER_HEIGHT )
        /// </summary>
        private void PlayerCollisionTest( )
            {

            if ( ( ball.Y > player.Y - PLAYER_HEIGHT ) && ( player.X <= ball.X ) && ( player.X + PLAYER_WIDTH >= ball.X + BALL_SIZE ) )
                {
                beepenator.Beep( "Player" );
                double mod = ball.X - ( player.X + 65 );
                ballSpeedX = ( mod / Math.Abs( mod ) ) * 0.3 + mod / ( PLAYER_WIDTH / 2 );
                ballSpeedY = -Math.Sqrt( 2 - ballSpeedX * ballSpeedX );
                }

            }

        private void PlayerMoving( )
            {
            if ( inputHandle.DPressed && ( player.X < WINDOW_WIDTH - PLAYER_WIDTH ) )
            { player.X += player.MoveSpeed; }

            if ( inputHandle.APressed && ( player.X > 0 ) )
            { player.X -= player.MoveSpeed; }

            if ( inputHandle.RightPressed && ( player.X < WINDOW_WIDTH ) )
            { player.X += player.MoveSpeed; }

            if ( inputHandle.LeftPressed && ( player.X > 0 ) )
            { player.X -= player.MoveSpeed; }
            }

        /// <summary>
        /// This method include ball moving
        /// and the Wall collision tester
        /// </summary>
        private void BallMoving( )
            {
            prevPosX = ball.X;
            prevPosY = ball.Y;

            ball.X += ( int )( ballSpeedX * ballSpeedModifer );
            ball.Y += ( int )( ballSpeedY * ballSpeedModifer );

            // Left
            if ( ball.X < 0 )
            { ballSpeedX = Math.Abs( ballSpeedX ); beepenator.Beep( "Wall" ); }
            // Right
            if ( ball.X > WINDOW_WIDTH - BALL_SIZE )
            { ballSpeedX = -Math.Abs( ballSpeedX ); beepenator.Beep( "Wall" ); }
            // Upper
            if ( ball.Y < 0 )
            { ballSpeedY = Math.Abs( ballSpeedY ); beepenator.Beep( "Wall" ); }
            // Bottom
            if ( ball.Y > WINDOW_HEIGHT - BALL_SIZE )
            { BallMissed( ); }

            }

        /// <summary>
        /// What happends if player miss a ball
        /// </summary>
        private void BallMissed( )
            {

            beepenator.Beep( "BallMissed" );

            if ( Game_Lives > 1 )
                {
                Game_Lives--;
                ball.X = WINDOW_WIDTH / 2;
                ball.Y = WINDOW_HEIGHT / 2;
                Game_State = "Idle";
                }
            else
                {
                Game_State = "Lose";

                if ( System.Windows.Forms.MessageBox.Show( "Вы проиграли", "Game", System.Windows.Forms.MessageBoxButtons.RetryCancel, System.Windows.Forms.MessageBoxIcon.Asterisk ) == System.Windows.Forms.DialogResult.Retry )
                    {

                    System.Windows.Forms.MessageBox.Show( "Вы набрали " + Game_Score + " очков!", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information );
               
                        Game_State = "Idle";
                        Game_Score = 0;
                        Game_Lives = 3;
                        player.X = ( WINDOW_WIDTH / 2 ) - ( PLAYER_WIDTH / 2 );

                        bricks = new List<Brick>( );

                        InitBricks( );

                    }
                else { this.Exit( ); }
                }
            }



        /// <summary>
        /// Create a array of bricks. 
        /// Ctor get a coordinates and some parameters
        /// to place bricks carefully.
        /// </summary>
        private void InitBricks( )
            {
            for ( int i = 0; i < BRICK_COLUMN; i++ )
                for ( int j = 0; j < BRICK_ROW; j++ )
                    bricks.Add( new Brick( Content, "brick1", ( 300 ) + ( j * 90 ), 120 + ( i * 30 ), BRICK_WIDTH, BRICK_HEIGHT ) );
            }


        private Player player;
        private Ball ball;
        private InputHandle inputHandle;
        private Beepenator beepenator;

        private List<Brick> bricks = new List<Brick>( );

        private Label scoreLabel;
        private Label livesLabel;
        private Label fpsLabel;

        private Label enterGameLabel;
        }
    }
