﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WindowsGame1
    {
    static class Fps
        {
        static private float elapsed_Time;
        static private int total_Frames;
        static private int fps = 0;
        /// <summary>
        ///     
        /// </summary>
        /// <param name="game">Instance of game class</param>
        /// <param name="gameTime">gameTime</param>
        static public int CheckFps (Game1 game, Microsoft.Xna.Framework.GameTime gameTime  )
            {
            
            elapsed_Time += ( float )gameTime.ElapsedGameTime.TotalMilliseconds;
            total_Frames++;

            // 1 Second has passed
            if ( elapsed_Time >= 1000.0f )
                {
                fps = total_Frames;
                total_Frames = 0;
                elapsed_Time = 0;
                }
            return fps;
            }
        }
    }
