﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace WindowsGame1
    {
    class Player
        {
        private Rectangle drawRectangle;
        private int moveSpeed;

        Texture2D sprite;


        public Player( ContentManager contentManager, string spriteName, int x, int y,int width, int height )
            {
            // load sprite, create rectangle
            LoadContent( contentManager, spriteName, x, y, width, height );

            moveSpeed = 12;
            }

        public int MoveSpeed
            {
            get { return moveSpeed; }
            set { moveSpeed = value; }
            }

        public int X
            {
            get { return drawRectangle.X; }
            set { drawRectangle.X = value; }
            }

        public int Y
            {
            get { return drawRectangle.Y; }
            set { drawRectangle.Y = value; }
            }

        public void Draw( SpriteBatch spriteBatch )
            {
            spriteBatch.Draw( sprite, drawRectangle, Color.White );
            }

        private void LoadContent( ContentManager contentManager, string spriteName, int x, int y, int width, int height )
            {
            sprite = contentManager.Load<Texture2D>( spriteName );
            drawRectangle = new Rectangle( x - sprite.Width / 2, y - sprite.Height / 2, width, height );
            }
        }
    }
