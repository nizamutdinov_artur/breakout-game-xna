﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace WindowsGame1
    {
    class Label
        {
        private SpriteFont font1;
        private Vector2 fontPos;


        public string Content { get; set; }

        /// <summary>
        /// Label ctor
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="text">Label text</param>
        /// <param name="instanceOfGame">Instance of base class</param>
        public Label( ContentManager contentManager, int x, int y, Game1 instanceOfGame,string text )
            {
            Content = text;

            font1 = contentManager.Load<SpriteFont>( "MyFont" );

            fontPos = new Vector2( x, y );
            }

        public Label( ContentManager contentManager, int x, int y, Game1 instanceOfGame )
            {
            Content = "Label";

            font1 = contentManager.Load<SpriteFont>( "MyFont" );

            fontPos = new Vector2( x, y );
            }

        public void Draw( SpriteBatch spriteBatch )
            {
            spriteBatch.DrawString( font1, Content, fontPos, Color.Black );
            }
        }
    }
