﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace WindowsGame1
    {
    public class Beepenator
        {
        /// <summary>
        /// This method have a parameters:
        /// </summary>
        /// <param name="param">Player, Brick, Wall, BallMissed</param>
        public void Beep( string param )
            {
            Thread myThread = new Thread(new ThreadStart(WallBeep));

            switch ( param )
                {
                case "Player":
                        {
                        myThread = new Thread( new ThreadStart( PlayerBeep ) );
                        break;
                        }
                case "Brick":
                            {
                            myThread = new Thread( new ThreadStart( BrickBeep ) );
                            break;
                            }

                case "Wall":
                                {
                                myThread = new Thread( new ThreadStart( WallBeep ) );
                                break;
                                }
                case "BallMissed":
                                    {
                                    myThread = new Thread( new ThreadStart( BallMissedBeep ) );
                                    break;
                                    }
                default: break;
                }

            myThread.Start( );

            }

        private void BrickBeep( )
            {
            Console.Beep( 1000, 30 );
            }
        private void PlayerBeep( )
            {
            Console.Beep( 500, 20 );
            }
        private void WallBeep( )
            {
            Console.Beep( 2000 , 40 );
            }
        private void BallMissedBeep( )
            {
            Console.Beep( 5000, 500 );
            }
        }
    }
