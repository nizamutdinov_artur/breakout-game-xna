﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{
    internal class MyLabel
    {
        private SpriteFont _font1;
        private Vector2 _fontPos;
        private Color _color;
        private string _content;


        /// <summary>
        /// Label constructor
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="text">Label text</param>
        /// <param name="color">Color of the label</param>
        public MyLabel(ContentManager contentManager, int x, int y, string text, Color color)
        {
            _color = color;

            Content = text;

            _font1 = contentManager.Load<SpriteFont>("MyFont");

            _fontPos = new Vector2(x, y);
        }

        /// <summary>
        /// Get or Set X coordinate
        /// </summary>
        public int X
            {
            get { return (int)_fontPos.X; }
            set { _fontPos.X = (float)value; }
            }

        /// <summary>
        /// Get or Set Y coordinate
        /// </summary>
        public int Y
            {
            get { return (int)_fontPos.Y; }
            set { _fontPos.Y = (float)value; }
            }

        /// <summary>
        ///  Get or Set text of the label
        /// </summary>
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Instance of main SpriteBatch class</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_font1, Content, _fontPos, _color);
        }
    }
}