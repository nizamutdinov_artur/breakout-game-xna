﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace WindowsGame1.GameObjects
{
    public class Ball
    {
        private Rectangle _rectangle;
        private Texture2D _sprite;

        /// <summary>
        /// Ball constructor
        /// </summary>
        /// <param name="contentManager">Instance of Content manager</param>
        /// <param name="spriteName">texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="radius">ball radius</param>
        public Ball(ContentManager contentManager, string spriteName, int x, int y, int radius)
        {
            // load sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _rectangle = new Rectangle(x, y, radius, radius);
        }


        /// <summary>
        /// Get or Set X coordinate
        /// </summary>
        public int X
        {
            get { return _rectangle.X; }
            set { _rectangle.X = value; }
        }

        /// <summary>
        /// Get or Set Y coordinate
        /// </summary>
        public int Y
        {
            get { return _rectangle.Y; }
            set { _rectangle.Y = value; }
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Instance of main SpriteBatch class</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _rectangle, Color.White);
        }

    }
}