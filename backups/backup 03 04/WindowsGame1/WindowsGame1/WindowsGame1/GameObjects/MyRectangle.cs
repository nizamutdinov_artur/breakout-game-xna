﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace WindowsGame1.GameObjects
{
    public class MyRectangle
    {
        private Texture2D _sprite;
        private Rectangle _rectangle;
        private float _opacity;

        private float _elapsed;
        private float _frameRate= 0.1f;

        /// <summary>
        /// MyRectangle constructor without image texture.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="width">Width of the rectangle.</param>
        /// <param name="height">Heigth of the rectangle.</param>
        /// <param name="graphicsDevice">The device.</param>
        /// <param name="color">Color of the rectangle</param>
        /// <param name="opacity">Opacity of the rectangle.</param>
        public MyRectangle(int x, int y, int width, int height, GraphicsDevice graphicsDevice, Color color, float opacity)
        {
            Width = width;
            Heigth = height;

            _opacity = opacity;

            var data = new Color[width*height];

            for (int i = 0; i < data.Length; i++)
            {
                data[i] = color;
            }

            _sprite = new Texture2D(graphicsDevice, width, height);
            _sprite.SetData(data);

            _rectangle = new Rectangle(x, y, width, height);

        }

        /// <summary>
        /// MyRectangle constructor with image texture.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="width">Width of the rectangle.</param>
        /// <param name="height">Height of the rectangle.</param>
        /// <param name="contentManager">Instance of Content Manager.</param>
        /// <param name="spriteName">Texture name.</param>
        /// <param name="opacity"></param>
        public MyRectangle( int x, int y, int width, int height, ContentManager contentManager, string spriteName, float opacity)
        {
            Width = width;
            Heigth = height;

            _opacity = opacity;

            // load _sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _rectangle = new Rectangle(x, y, width, height);

        }

        public int Width { get; set; }

        public int Heigth { get; set; }

        /// <summary>
        /// Get or Set X coordinate
        /// </summary>
        public int X
        {
            get { return _rectangle.X; }
            set { _rectangle.X = value; }
        }

        /// <summary>
        /// Get or Set Y coordinate
        /// </summary>
        public int Y
        {
            get { return _rectangle.Y; }
            set { _rectangle.Y = value; }
        }

        /// <summary>
        /// Get or Set Opacity of the rectangle.
        /// </summary>
        public float Opacity
        {
            get { return _opacity; }
            set { _opacity = value; }
        }


        public void FadeOpacity(GameTime gametime, bool opacityUp)
        {
            if (opacityUp)
            {
                _elapsed += (float) gametime.ElapsedGameTime.TotalSeconds;

                // Until it passes our frame length
                if (_elapsed > _frameRate)
                {
                    if (_opacity <= 1.0f)
                    {
                        _opacity += 0.05f;
                    }

                    // Reset the elapsed frame time.
                    _elapsed = 0.0f;

                }
            } else
            {
                _elapsed += (float) gametime.ElapsedGameTime.TotalSeconds;

                // Until it passes our frame length
                if (_elapsed > _frameRate)
                {
                    if (_opacity <= 0.0f)
                    {
                        _opacity -= 0.001f;
                    }

                    // Reset the elapsed frame time.
                    _elapsed = 0.0f;

                }
            }
        }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Instance of main SpriteBatch class</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _rectangle, Color.White*_opacity);
        }
    }
}