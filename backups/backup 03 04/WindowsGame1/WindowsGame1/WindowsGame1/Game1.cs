﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Media;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Threading;
using System.Windows.Forms;
using WindowsGame1.GameObjects;
using Microsoft.Win32;

namespace WindowsGame1
{

    public class Game1 : Microsoft.Xna.Framework.Game
    {
        #region Consts

        // Settings
        private const string GameName = "BREAKOUT";
        private const int GameFps = 75;

        // Parameters number of bricks.
        private const int BrickRow = 10;
        private const int BrickColumn = 6;

        // Window size.
        public const int WindowWidth = 800;
        public const int WindowHeight = 600;

        // Size of game objects.
        private const int PlayerWidth = 100;
        private const int PlayerHeight = 10;
        public const int BrickWidth = 80;
        public const int BrickHeight = 15;
        public const int BallSize = 15;


        // Pause Menu sizes
        private const int PauseWidth = 300;
        private const int PauseHeight = 200;
        private const int PauseButtonWidth = 150;
        private const int PauseButtonHeigth = 40;

        // Sprite names
        public const string GameBrickRed = "brickRed";
        public const string GameBrickOrange1 = "brickOrange1";
        public const string GameBrickOrange2 = "brickOrange2";
        public const string GameBrickYellow = "brickYellow";
        public const string GameBrickGreen = "brickGreen";
        public const string GameBrickBlue = "brickBlue";

        private const string GameBallSprite = "ball1";
        private const string GamePlayerSprite = "player1";

        // TODO:
        private const int GameBrickRedPoints = 7;
        private const int GameBrickOrangePoints = 5;
        private const int GameBrickYellowPoints = 3;
        private const int GameBrickGreenPoints = 2;
        private const int GameBrickBluePoints = 1;

        #endregion

        #region Fields

        // Instances of classes XNA.
        public GraphicsDeviceManager graphics;
        public SpriteBatch spriteBatch;

        // Game tools.
        private bool ArtificialIntelligence = false;
        private int fps;
        private Random _random;

        // Game status.
        private int gameLives = 3;
        private int gameScore = 0;
        private int gameStage = 1;
        private string gameState = "Intro";

        // Settings
        public bool gameSound = true;

        // Ball props.
        private double ballSpeedX = 1;
        private double ballSpeedY = 1;
        private float ballSpeedMultiplier = 60f;
        private int ballToBrickCollisionCounter =0; // TODO: AFTER 4 TOUCHES SPEDOOS ++

        // Intro
        private MyRectangle introBackground;

        // Game Menu

        private MyRectangle menuBgRect;
        private MyRectangle menuStartRect;
        private MyRectangle menuAboutRect;
        private MyRectangle menuExitRect;

        // Pause Menu
        private MyRectangle pauseBackgroundRectangle;
        private MyRectangle pauseRectangle;
        private MyRectangle pauseRectangleUpper;

        private MyRectangle continueButtonRectangle;
        private MyRectangle gotoMainButtonRectangle;
        private MyRectangle exitButtonRectangle;

        private MyLabel continueButtonLabel;
        private MyLabel gotoMainButtonLabel;
        private MyLabel exitButtonLabel;
        private MyLabel pauseLabel;

        // Game objects.
        private Player player;
        private Ball ball;

        // An array containing all the bricks.
        private List<Brick> bricks = new List<Brick>();

        // Game Labels.
        private MyLabel scoreLabel;
        private MyLabel livesLabel;
        private MyLabel _fpsLabel;
        private MyLabel enterGameLabel;


        #endregion

        /// <summary>
        /// The "Game1" class constructor.
        /// </summary>
        public Game1()
        {
            // Create new GraphicsDeviceManager.
            graphics = new GraphicsDeviceManager(this);

            // Specify the folder with resources.
            Content.RootDirectory = "Content";

            // Set game TITLE.
            this.Window.Title = GameName;

            // Mouse visible.
            this.IsMouseVisible = true;

            // Window resolution.
            graphics.PreferredBackBufferWidth = WindowWidth;
            graphics.PreferredBackBufferHeight = WindowHeight;

            // Set fps from GAME_FPS constant.
            this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f/(float) GameFps);
        }

        /// <summary>
        /// Overrides method from parent class
        /// </summary>
        protected override void Initialize()
        {
            _random = new Random();

            base.Initialize();
        }

        /// <summary>
        /// Overrides method from parent class
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // Intro
            introBackground = new MyRectangle(0, 0, WindowWidth, WindowHeight, Content, "intro", 0.0f);

            // Game menu
            menuBgRect = new MyRectangle(0, 0, WindowWidth, WindowHeight, Content, "menu", 1.0f);

            menuStartRect = new MyRectangle(0, 228, WindowWidth, 45, graphics.GraphicsDevice, Color.White, 0.0f);
            menuAboutRect = new MyRectangle(0, 300, WindowWidth, 45, graphics.GraphicsDevice, Color.White, 0.0f);
            menuExitRect = new MyRectangle(0, 368, WindowWidth, 45, graphics.GraphicsDevice, Color.White, 0.0f);


            // Pause menu
            pauseBackgroundRectangle = new MyRectangle(0, 0, WindowWidth, WindowHeight, graphics.GraphicsDevice, Color.Black, 0.8f);
            pauseRectangle = new MyRectangle((WindowWidth/2) - (PauseWidth/2), (WindowHeight/2) - (PauseHeight/2), PauseWidth, PauseHeight, graphics.GraphicsDevice, Color.WhiteSmoke, 0.9f);
            pauseRectangleUpper = new MyRectangle((WindowWidth/2) - (PauseWidth/2), (WindowHeight/2) - (PauseHeight/2) - PauseHeight/5, PauseWidth, PauseHeight/6, graphics.GraphicsDevice, Color.WhiteSmoke, 0.9f);

            continueButtonRectangle = new MyRectangle((pauseRectangle.X) + (PauseWidth/2) - (PauseButtonWidth/2), pauseRectangle.Y + 20, PauseButtonWidth, PauseButtonHeigth, graphics.GraphicsDevice, Color.Black, 0.9f);
            gotoMainButtonRectangle = new MyRectangle((pauseRectangle.X) + (PauseWidth/2) - (PauseButtonWidth/2), continueButtonRectangle.Y + 60, PauseButtonWidth, PauseButtonHeigth, graphics.GraphicsDevice, Color.Black, 0.9f);
            exitButtonRectangle = new MyRectangle((pauseRectangle.X) + (PauseWidth/2) - (PauseButtonWidth/2), gotoMainButtonRectangle.Y + 60, PauseButtonWidth, PauseButtonHeigth, graphics.GraphicsDevice, Color.Black, 0.9f);

            pauseLabel = new MyLabel(Content, pauseRectangleUpper.X + 130, pauseRectangleUpper.Y + 6, "PAUSE", Color.Black);
            continueButtonLabel = new MyLabel(Content, continueButtonRectangle.X + 20, continueButtonRectangle.Y + 5, "Continue", Color.White);
            gotoMainButtonLabel = new MyLabel(Content, continueButtonRectangle.X + 20, continueButtonLabel.Y + 60, "Main menu", Color.White);
            exitButtonLabel = new MyLabel(Content, continueButtonRectangle.X + 20, gotoMainButtonLabel.Y + 60, "Exit", Color.White);


            // Game objects.
            player = new Player(Content, GamePlayerSprite, (WindowWidth/2) - (PlayerWidth/2), WindowHeight - 15, PlayerWidth, PlayerHeight);
            ball = new Ball(Content, GameBallSprite, WindowWidth/2, WindowHeight/2, BallSize);
            InitBricks();

            // Labels.
            livesLabel = new MyLabel(Content, WindowWidth - 100, 10, "", Color.White);
            scoreLabel = new MyLabel(Content, 10, 10, "", Color.White);
            enterGameLabel = new MyLabel(Content, WindowHeight/2, WindowWidth/2 - 100, "Press space to game", Color.White);
            _fpsLabel = new MyLabel(Content, 300, 10, "", Color.White);

        }

        /// <summary>
        /// Runs every frame.
        /// </summary>
        /// <param name="gameTime">Instance of gameTime.</param>
        protected override void Update(GameTime gameTime)
        {
            // Pause the game, if it is not active.
            if (!IsActive)
            {
                return;
            }

            // Update the status of the keyboard.
            MyKeyState.CheckState(gameTime);

            // Get Number of fps.
            fps = Fps.CheckFps(gameTime);

            // Set labels content.
            livesLabel.Content = "Lives: " + gameLives;
            scoreLabel.Content = "Score: " + gameScore;
            _fpsLabel.Content = "FPS: " + fps;

            switch (gameState)
            {
                case "Game":
                {
                    PlayerMoving();
                    BallMoving();

                    PlayerCollisionTest();
                    BrickCollisionTest();
                    break;
                }
                case "Idle":
                {
                    PlayerMoving();
                    // The ball follows the player.
                    ball.X = player.X + (PlayerWidth/2) - BallSize/2;
                    ball.Y = player.Y - BallSize;
                    // Handle keypress event beginning of the game
                    if (MyKeyState.SpacePressed == true)
                    {
                        gameState = "Game";
                    }
                    break;
                }
                case "Menu":
                {

                    PseudoAI();

                    MySound.Sound = false;
                    ArtificialIntelligence = true;
                    BallMoving();


                    PlayerCollisionTest();
                    BrickCollisionTest();

                    MenuController( );
                    break;
                }
                case "Pause":
                {
                    PauseController();
                    break;
                }
                case "Intro":
                {
                    if (MyKeyState.EnterPressed)
                    {
                        gameState = "Menu";

                    }

                    introBackground.FadeOpacity(gameTime, true);
                    break;
                }
            }


            base.Update(gameTime);
        }


        /// <summary>
        /// Override a default XNA Draw method 
        /// </summary>
        /// <param name="gameTime">Gametime</param>
        protected override void Draw(GameTime gameTime)
        {
            // Fills the screen black.
            GraphicsDevice.Clear(Color.Black);
            // Begin draw.
            spriteBatch.Begin();

            switch (gameState)
            {
                case "Game":
                {
                    if (MyKeyState.EscPressed) { gameState = "Pause"; }
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();

                    scoreLabel.Draw(spriteBatch);
                    livesLabel.Draw(spriteBatch);
                    _fpsLabel.Draw(spriteBatch);
                    break;
                }
                case "Idle":
                {
                    if (MyKeyState.EscPressed) { gameState = "Pause"; }
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();
                    enterGameLabel.Draw(spriteBatch);

                    scoreLabel.Draw(spriteBatch);
                    livesLabel.Draw(spriteBatch);
                    _fpsLabel.Draw(spriteBatch);
                    break;
                }
                case "Menu":
                {
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();

                    menuBgRect.Draw(spriteBatch);
                    menuStartRect.Draw(spriteBatch);
                    menuAboutRect.Draw(spriteBatch);
                    menuExitRect.Draw(spriteBatch);
                    break;
                }
                case "Pause":
                {
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();

                    pauseBackgroundRectangle.Draw(spriteBatch);
                    pauseRectangle.Draw(spriteBatch);
                    pauseRectangleUpper.Draw(spriteBatch);

                    continueButtonRectangle.Draw(spriteBatch);
                    gotoMainButtonRectangle.Draw(spriteBatch);
                    exitButtonRectangle.Draw(spriteBatch);

                    continueButtonLabel.Draw(spriteBatch);
                    gotoMainButtonLabel.Draw(spriteBatch);
                    exitButtonLabel.Draw(spriteBatch);
                    pauseLabel.Draw(spriteBatch);
                    break;
                }
                case "Intro":
                {
                    introBackground.Draw(spriteBatch);
                    break;
                }
            }


            // End draw.
            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// TODO:
        /// </summary>
        private void MenuController()
        {

            menuStartRect.Opacity = 0.0f;
            menuAboutRect.Opacity = 0.0f;
            menuExitRect.Opacity = 0.0f;

            if (Collision.Test(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, menuStartRect.X, menuStartRect.Y, menuStartRect.Width, menuStartRect.Heigth))
            {
                menuStartRect.Opacity = 0.5f;

                if (MyKeyState.LMousePressed)
                {
                    MySound.Sound = true;
                    gameState = "Idle";
                    ResetGame();

                }
            }


            if (Collision.Test(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, menuAboutRect.X, menuAboutRect.Y, menuAboutRect.Width, menuAboutRect.Heigth))
            {
                menuAboutRect.Opacity = 0.5f;

                if (MyKeyState.LMousePressed)
                {
                    // TODO:  
                }
            }

            if (Collision.Test(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, menuExitRect.X, menuExitRect.Y, menuExitRect.Width, menuExitRect.Heigth))
            {
                menuExitRect.Opacity = 0.5f;

                if (MyKeyState.LMousePressed)
                {
                    if (System.Windows.Forms.MessageBox.Show("ORLY ?", "Game", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Exit();
                    }
                }

            }
        }

        /// <summary>
        /// TODO:
        /// </summary>
        private void PauseController()
        {
            if (MyKeyState.LMousePressed && Collision.Test(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, continueButtonRectangle.X, continueButtonRectangle.Y, continueButtonRectangle.Width, continueButtonRectangle.Heigth))
            {
                gameState = "Game";

            }

            if (MyKeyState.LMousePressed && Collision.Test(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, gotoMainButtonRectangle.X, gotoMainButtonRectangle.Y, gotoMainButtonRectangle.Width, gotoMainButtonRectangle.Heigth))
            {
                gameState = "Menu";

            }

            if (MyKeyState.LMousePressed && Collision.Test(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, exitButtonRectangle.X, exitButtonRectangle.Y, exitButtonRectangle.Width, exitButtonRectangle.Heigth))
            {
                if (System.Windows.Forms.MessageBox.Show("ORLY ?", "Game", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    Exit();
                }
            }
        }

        /// <summary>
        /// TODO: AI.
        /// </summary>
        private void PseudoAI()
        {
            if (MyKeyState.EnterPressed)
            {
                ArtificialIntelligence = true;
            }

            if (ArtificialIntelligence && ball.Y < WindowHeight - 40)
            {
                player.X = (ball.X) - (PlayerWidth/2); // Crazy >__<  player.X = (ball.X + _random.Next(-50,50)) - ( PlayerWidth / 2 );
            }
        }

        /// <summary>
        /// Draws the bricks from the array List.
        /// </summary>
        private void DrawBricks()
        {
            for (int i = 0; i < bricks.Count(); i++)
                if (bricks[i] != null)
                    bricks[i].Draw(spriteBatch);
        }

        /// <summary>
        /// Checks collision bricks with the ball.
        /// </summary>
        private void BrickCollisionTest()
        {
            for (int i = 0; i < bricks.Count(); i++)
            {
                if (bricks[i] != null &&
                    Collision.Test(bricks[i].X, bricks[i].Y, BrickWidth, BrickHeight, ball.X, ball.Y, BallSize, BallSize))
                {
                    MySound.Beep("Brick");

                    // Find the angle between center of brick and a ball center.
                    int angle = (int) (Math.Atan2((bricks[i].Y + BrickHeight/2) - (ball.Y + BallSize/2),
                        (bricks[i].X + BrickWidth/2) - (ball.X + BallSize/2))/Math.PI*180);

                    if (angle < 10 && angle > -10) // Angles between -10 and 10.
                        ballSpeedX = -Math.Abs(ballSpeedX);
                    else if ((angle < 180 && angle > 170) || (angle > -180 && angle < -170)) // Angles between 180 and -170 
                        ballSpeedX = Math.Abs(ballSpeedX);
                    else if (angle > 10 && angle < 170)
                        ballSpeedY = -Math.Abs(ballSpeedY);
                    else if (angle < -10 && angle > -170)
                        ballSpeedY = Math.Abs(ballSpeedY);

                    switch (bricks[i].SpriteName)
                    {
                        case GameBrickRed:
                            // Set random speed multiplier
                            ballSpeedMultiplier += _random.Next(1, 10);
                            gameScore += GameBrickRedPoints;
                            break;
                        case GameBrickOrange1:
                            gameScore += GameBrickOrangePoints;
                            break;
                        case GameBrickOrange2:
                            gameScore += GameBrickOrangePoints;
                            break;
                        case GameBrickYellow:
                            gameScore += GameBrickYellowPoints;
                            break;
                        case GameBrickGreen:
                            gameScore += GameBrickGreenPoints;
                            break;
                        case GameBrickBlue:
                            gameScore += GameBrickBluePoints;
                            break;
                        default:
                            gameScore += 0;
                            break;
                    }

                    // TODO: POWER PACK

                    bricks[i] = null;
                    break;
                }
            }
        }

        /// <summary>
        /// Checks collision player with ball.
        /// </summary>
        private void PlayerCollisionTest()
        {
            if (Collision.Test(player.X, player.Y, PlayerWidth, PlayerHeight, ball.X, ball.Y, BallSize, BallSize))
            {
                MySound.Beep("Player");
                double difference = ball.X - (player.X + PlayerWidth/2);
                ballSpeedX = ( difference / Math.Abs( difference ) ) * 0.7 + difference / ( PlayerWidth );  //arccos((x-0.5l)/0.5l)    Indus c0de         ( difference / Math.Abs( difference ) ) * 0.7 + difference / ( PlayerWidth )
                ballSpeedY = -Math.Sqrt(2.5 - ballSpeedX*ballSpeedX);
            }

        }

        /// <summary>
        /// Player moves.
        /// </summary>
        private void PlayerMoving()
        {
            if (MyKeyState.DPressed && (player.X < WindowWidth - PlayerWidth))
            {
                player.X += player.MoveSpeed;
            }

            if (MyKeyState.APressed && (player.X > 0))
            {
                player.X -= player.MoveSpeed;
            }

            if (MyKeyState.RightPressed && (player.X < WindowWidth))
            {
                player.X += player.MoveSpeed;
            }

            if (MyKeyState.LeftPressed && (player.X > 0))
            {
                player.X -= player.MoveSpeed;
            }
        }

        /// <summary>
        /// This method include ball moving
        /// and the Wall collision tester.
        /// </summary>
        private void BallMoving()
        {
            ball.X += (int) (ballSpeedX*(ballSpeedMultiplier/10));
            ball.Y += (int) (ballSpeedY*(ballSpeedMultiplier/10));

            ballSpeedMultiplier -= 0.01f;

            // Left
            if (ball.X < 0)
            {
                ballSpeedX = Math.Abs(ballSpeedX);
                MySound.Beep("Wall");
            }
            // Right
            if (ball.X > WindowWidth - BallSize)
            {
                ballSpeedX = -Math.Abs(ballSpeedX);
                MySound.Beep("Wall");
            }
            // Upper
            if (ball.Y < 0)
            {
                ballSpeedY = Math.Abs(ballSpeedY);
                MySound.Beep("Wall");
            }
            // Bottom
            if (ball.Y > WindowHeight - BallSize)
            {
                BallMissed();
            }

        }

        /// <summary>
        /// What happends if player miss the ball.
        /// </summary>
        private void BallMissed()
        {

            MySound.Beep("BallMissed");

            if (gameLives > 1)
            {
                // Take live.
                gameLives--;
                // Set speed to default.
                ballSpeedMultiplier = 60;
                // Set a game state to Idle.
                gameState = "Idle";
            } else
            {
                // Displays a dialog window to continue the game or exit.
                if (System.Windows.Forms.MessageBox.Show("Вы проиграли.\n Ваш счет: " + gameScore + " Очков", "Game", System.Windows.Forms.MessageBoxButtons.RetryCancel, System.Windows.Forms.MessageBoxIcon.Asterisk) == System.Windows.Forms.DialogResult.Retry)
                {
                   ResetGame();
                } else
                {
                    Exit();
                }
            }
        }

        /// <summary>
        /// Reloads game.
        /// </summary>
        private void ResetGame()
        {
            ballSpeedMultiplier = 60;
            gameState = "Idle";
            gameScore = 0;
            gameLives = 3;
            // Set the position of the player in the center of the screen.
            player.X = (WindowWidth/2) - (PlayerWidth/2);
            // Clear all the bricks from the stage.
            bricks = new List<Brick>();

            InitBricks();

        }

        /// <summary>
        /// Puts bricks on stage.
        /// </summary>
        private void InitBricks()
        {
            for (int i = 0; i < BrickColumn; i++)
                for (int j = 0; j < BrickRow; j++)
                {
                    const int marginX = 0;
                    const int marginY = 50;

                    int distanceX = j*80;
                    int distanceY = i*15;

                    switch (i)
                    {
                        case 0:
                        {
                            bricks.Add(new Brick(Content, GameBrickRed, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                        case 1:
                        {
                            bricks.Add(new Brick(Content, GameBrickOrange1, marginX + distanceX, marginY + distanceY, BrickWidth,
                                BrickHeight));
                            break;
                        }
                        case 2:
                        {
                            bricks.Add(new Brick(Content, GameBrickOrange2, marginX + distanceX, marginY + distanceY, BrickWidth,
                                BrickHeight));
                            break;
                        }
                        case 3:
                        {
                            bricks.Add(new Brick(Content, GameBrickYellow, marginX + distanceX, marginY + distanceY, BrickWidth,
                                BrickHeight));
                            break;
                        }
                        case 4:
                        {
                            bricks.Add(new Brick(Content, GameBrickGreen, marginX + distanceX, marginY + distanceY, BrickWidth,
                                BrickHeight));
                            break;
                        }
                        case 5:
                        {
                            bricks.Add(new Brick(Content, GameBrickBlue, marginX + distanceX, marginY + distanceY, BrickWidth,
                                BrickHeight));
                            break;
                        }
                    }
                }

        }

    }
}
