﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;

namespace WindowsGame1
{  
    /// <summary>
    /// TODO: First use
    /// </summary>
    public class AnimatedSprite
    {
        private Texture2D _texture;

        private float _frameRate = 0.02f;
        private int _frameCount;
        private int _currentFrame;

        private float _elapsed = 0.0f;

        private int _x;
        private int _y;

        private int _frameWidth;
        private int _frameHeight;

        private int _frameOffsetX;
        private int _frameOffsetY;

        private bool _animating = true;

        public AnimatedSprite(ContentManager content, string spriteName, int frameOffsetX, int frameOffsetY, int frameWidth, int frameHeight, int frameCount)
        {
            _texture = content.Load<Texture2D>(spriteName);

            _frameOffsetX = frameOffsetX;
            _frameOffsetY = frameOffsetY;
            _frameWidth = frameWidth;
            _frameHeight = frameHeight;
            _frameCount = frameCount;
        }

        public void Update(GameTime gameTime)
        {
            _elapsed += (float) gameTime.ElapsedGameTime.Milliseconds;
            if ( _elapsed>_frameRate)
            {
                if (_animating)
                {
                    if (_currentFrame == _frameCount) { _currentFrame = 0;}
                    _currentFrame++;
                }
                _elapsed = 0.0f;
            }

        }

        public int X
        {
            get { return _x; }
            set { _x = value; }
        }

        public int Y
        {
            get { return _y; }
            set { _y = value; }
        }

        /// <summary>
        /// Get or Set (from 0 to _frameCount) Current Frame
        /// </summary>
        public int Frame
        {
            get { return _currentFrame; }
            set { _currentFrame = (int) MathHelper.Clamp(value, 0, _frameCount); }
        }

        public float FrameLength
        {
            get { return _frameRate; }
            set { _frameRate = (float) Math.Max(value, 0f); }
        }

        public bool IsAnimating
        {
            get { return _animating; }
            set { _animating = value; }
        }

        public Rectangle GetCurrentFrameRect  ()
        {
            return new Rectangle(_frameOffsetX + (_frameWidth*_currentFrame), _frameOffsetY, _frameWidth, _frameHeight);
        }
    }
}
