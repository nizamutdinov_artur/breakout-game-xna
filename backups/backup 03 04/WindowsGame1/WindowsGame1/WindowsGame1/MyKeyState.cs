﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;

namespace WindowsGame1
{
    /// <summary>
    /// TODO: Write comments
    /// </summary>
    public static class MyKeyState
    {
        
        private static bool _aPressed = false;
        private static bool _dPressed = false;
        private static bool _leftPressed = false;
        private static bool _rightPressed = false;

        private static bool _enterPressed = false;
        private static bool _spacePressed = false;
        private static bool _escPressed = false;
        private static bool _lMousePressed = false;

        #region Public

        public static bool APressed
        {
            get { return _aPressed; }
            private set { _aPressed = value; }
        }

        public static bool DPressed
        {
            get { return _dPressed; }
            private set { _dPressed = value; }
        }

        public static bool LeftPressed
        {
            get { return _leftPressed; }
            private set { _leftPressed = value; }
        }

        public static bool RightPressed
        {
            get { return _rightPressed; }
            private set { _rightPressed = value; }
        }

        public static bool EnterPressed
        {
            get { return _enterPressed; }
            private set { _enterPressed = value; }
        }

        public static bool SpacePressed
        {
            get { return _spacePressed; }
            private set { _spacePressed = value; }
        }

        public static bool LMousePressed
        {
            get { return _lMousePressed; }
            set { _lMousePressed = value; }
        }

        public static bool EscPressed
        {
            get { return _escPressed; }
            set { _escPressed = value; }
        }

        #endregion

        public static void CheckState(GameTime gameTime)
        {
        KeyboardState keybState = Keyboard.GetState( );

            _leftPressed = keybState.IsKeyDown(Keys.Left);
            _rightPressed = keybState.IsKeyDown(Keys.Right);
            _aPressed = keybState.IsKeyDown( Keys.A );
            _dPressed = keybState.IsKeyDown( Keys.D );

            _enterPressed = keybState.IsKeyDown( Keys.Enter );
            _spacePressed = keybState.IsKeyDown( Keys.Space );
            _escPressed = keybState.IsKeyDown( Keys.Escape );

            _lMousePressed = Mouse.GetState().LeftButton == ButtonState.Pressed;

        }
    }
}