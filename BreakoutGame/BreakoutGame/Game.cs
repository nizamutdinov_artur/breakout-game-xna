﻿using System;
using System.Collections.Generic;
using System.Linq;
using BreakoutGame.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutGame
{

    /// <summary>
    /// TODO: GAME OVER, PIXEL HEARTS, 8 BIT EXPLOSION STYLE, SCREENSAVER CLASS
    /// 230 POINTS MAX
    /// First part of the game class.
    /// Const, fields, ctor, init, update, draw.
    /// </summary>
    public partial class MyGame : Game
    {
        #region Consts

        // Settings
        private const string GameName = "BREAKOUT";
        private const float GameFps = 60f;

        // Parameters number of bricks.
        private const int BrickRow = 10;
        private const int BrickColumn = 6;

        // Window size.
        /// <summary>
        /// The window width
        /// </summary>
        public const int WindowWidth = 800;
        /// <summary>
        /// The window height
        /// </summary>
        public const int WindowHeight = 600;

        // Size of game objects.
        /// <summary>
        /// The player width
        /// </summary>
        public const int PlayerWidth = 100;
        /// <summary>
        /// The player height
        /// </summary>
        public const int PlayerHeight = 10;
        /// <summary>
        /// The brick width
        /// </summary>
        public const int BrickWidth = 80;
        /// <summary>
        /// The brick height
        /// </summary>
        public const int BrickHeight = 15;
        /// <summary>
        /// The ball size
        /// </summary>
        public const int BallSize = 15;
        /// <summary>
        /// The explosion size
        /// </summary>
        public const int ExplosionSize = 64;


        // Pause Menu sizes
        private const int PauseWidth = 300;
        private const int PauseHeight = 200;
        private const int PauseButtonWidth = 150;
        private const int PauseButtonHeight = 40;

        // Sprite names

        /// <summary>
        /// Sprite name.
        /// </summary>
        public const string GameBrickRed = "Bricks\\brickRed";
        /// <summary>
        /// Sprite name.
        /// </summary>
        public const string GameBrickOrange1 = "Bricks\\brickOrange1";
        /// <summary>
        /// Sprite name.
        /// </summary>
        public const string GameBrickOrange2 = "Bricks\\brickOrange2";
        /// <summary>
        /// Sprite name.
        /// </summary>
        public const string GameBrickYellow = "Bricks\\brickYellow";
        /// <summary>
        /// Sprite name.
        /// </summary>
        public const string GameBrickGreen = "Bricks\\brickGreen";
        /// <summary>
        /// Sprite name.
        /// </summary>
        public const string GameBrickBlue = "Bricks\\brickBlue";

        private const string GameBallSprite = "GameObject\\ball1";
        private const string GamePlayerSprite = "GameObject\\player1";

        // Number of the points given from specified brick
        private const int GameBrickRedPoints = 7;
        private const int GameBrickOrangePoints = 5;
        private const int GameBrickYellowPoints = 3;
        private const int GameBrickGreenPoints = 2;
        private const int GameBrickBluePoints = 1;

        #endregion

        #region Fields

        // Instances of classes XNA.
        /// <summary>
        /// The graphics
        /// </summary>
        public GraphicsDeviceManager graphics;
        /// <summary>
        /// The sprite batch
        /// </summary>
        public SpriteBatch spriteBatch;

        // Game tools.
        private int fps;
        private Random random;

        // Game status.
        private int gameLives = 3;
        private int gameScore = 0;

        // Settings
        /// <summary>
        /// The game sound
        /// </summary>
        public bool gameSound = true;
        private GameStates gameState;

        // Ball props.
        private double ballVelocityX = 1;
        private double BallVelocityY = 1;
        private float ballSpeedMultiplier = 60f;
        private int ballToBrickCollisionCounter; // After 4 touches speed up

        // Intro
        private MyRectangle introBackground;

        // Game Menu

        private MyRectangle menuBgRect;
        private MyRectangle menuStartRect;
        private MyRectangle menuAboutRect;
        private MyRectangle menuExitRect;

        // Pause Menu
        private MyRectangle pauseBackgroundRectangle;
        private MyRectangle pauseRectangle;
        private MyRectangle pauseRectangleUpper;

        private MyRectangle continueButtonRectangle;
        private MyRectangle gotoMainButtonRectangle;
        private MyRectangle exitButtonRectangle;

        private MyLabel continueButtonLabel;
        private MyLabel gotoMainButtonLabel;
        private MyLabel exitButtonLabel;
        private MyLabel pauseLabel;

        // Game objects.

        /// <summary>
        /// The player
        /// </summary>
        public Player player;

        /// <summary>
        /// The ball
        /// </summary>
        public Ball ball;

        private PowerPackManager _powerPackModel;

        // An array containing all the bricks.
        private List<Brick> bricks = new List<Brick>();

        // Game Labels.
        private MyLabel scoreLabel;
        private MyLabel livesLabel;
        private MyLabel fpsLabel;
        private MyLabel enterGameLabel;

        // Sound
        private SoundFactory _soundFactory;

        #endregion

        /// <summary>
        /// The "Game" class constructor.
        /// </summary>
        public MyGame()
        {
            // Create new GraphicsDeviceManager.
            graphics = new GraphicsDeviceManager(this);

            // Specify the folder with resources.
            Content.RootDirectory = "Content";

            // Set game TITLE.
            this.Window.Title = GameName;

            // Mouse visible.
            this.IsMouseVisible = true;

            // Window resolution.
            graphics.PreferredBackBufferWidth = WindowWidth;
            graphics.PreferredBackBufferHeight = WindowHeight;

            // Set fps from GAME_FPS constant.
            this.TargetElapsedTime = TimeSpan.FromSeconds(1.0f/ GameFps);
        }

        /// <summary>
        /// Overrides method from parent class
        /// </summary>
        protected override void Initialize()
        {
            gameState = GameStates.Intro;
            random = new Random();

            base.Initialize();
        }

        /// <summary>
        /// Overrides method from parent class
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            // Intro
            introBackground = new MyRectangle( Content, "Background\\intro", 0, 0, WindowWidth, WindowHeight, 0.0f );

            // Game menu
            menuBgRect = new MyRectangle( Content, "Background\\menu", 0, 0, WindowWidth, WindowHeight, 1.0f );

            menuStartRect = new MyRectangle(0, 228, WindowWidth, 45, graphics.GraphicsDevice, Color.White, 0.0f);
            menuAboutRect = new MyRectangle(0, 300, WindowWidth, 45, graphics.GraphicsDevice, Color.White, 0.0f);
            menuExitRect = new MyRectangle(0, 368, WindowWidth, 45, graphics.GraphicsDevice, Color.White, 0.0f);


            // Pause menu
            pauseBackgroundRectangle = new MyRectangle(0, 0, WindowWidth, WindowHeight, graphics.GraphicsDevice, Color.Black, 0.8f);
            pauseRectangle = new MyRectangle((WindowWidth/2) - (PauseWidth/2), (WindowHeight/2) - (PauseHeight/2), PauseWidth, PauseHeight, graphics.GraphicsDevice, Color.WhiteSmoke, 0.9f);
            pauseRectangleUpper = new MyRectangle((WindowWidth/2) - (PauseWidth/2), (WindowHeight/2) - (PauseHeight/2) - PauseHeight/5, PauseWidth, PauseHeight/6, graphics.GraphicsDevice, Color.WhiteSmoke, 0.9f);

            continueButtonRectangle = new MyRectangle((pauseRectangle.X) + (PauseWidth/2) - (PauseButtonWidth/2), pauseRectangle.Y + 20, PauseButtonWidth, PauseButtonHeight, graphics.GraphicsDevice, Color.Black, 0.9f);
            gotoMainButtonRectangle = new MyRectangle((pauseRectangle.X) + (PauseWidth/2) - (PauseButtonWidth/2), continueButtonRectangle.Y + 60, PauseButtonWidth, PauseButtonHeight, graphics.GraphicsDevice, Color.Black, 0.9f);
            exitButtonRectangle = new MyRectangle((pauseRectangle.X) + (PauseWidth/2) - (PauseButtonWidth/2), gotoMainButtonRectangle.Y + 60, PauseButtonWidth, PauseButtonHeight, graphics.GraphicsDevice, Color.Black, 0.9f);

            pauseLabel = new MyLabel(Content, pauseRectangleUpper.X + 130, pauseRectangleUpper.Y + 6, "PAUSE", Color.Black);
            continueButtonLabel = new MyLabel(Content, continueButtonRectangle.X + 20, continueButtonRectangle.Y + 5, "Continue", Color.White);
            gotoMainButtonLabel = new MyLabel(Content, continueButtonRectangle.X + 20, continueButtonLabel.Y + 60, "Main menu", Color.White);
            exitButtonLabel = new MyLabel(Content, continueButtonRectangle.X + 20, gotoMainButtonLabel.Y + 60, "Exit", Color.White);


            // Game objects.
            player = new Player(Content, GamePlayerSprite, (WindowWidth/2) - (PlayerWidth/2), WindowHeight - 15, PlayerWidth, PlayerHeight);
            ball = new Ball(Content, GameBallSprite, WindowWidth/2, WindowHeight/2, BallSize);
            InitBricks();

            _powerPackModel = new PowerPackManager(Content);

            // Labels.
            livesLabel = new MyLabel(Content, WindowWidth - 100, 10, "", Color.White);
            scoreLabel = new MyLabel(Content, 10, 10, "", Color.White);
            enterGameLabel = new MyLabel(Content, WindowHeight/2, WindowWidth/2 - 100, "Press space to game", Color.White);
            fpsLabel = new MyLabel(Content, 300, 10, "", Color.White);

            // Sound
            _soundFactory = new SoundFactory(Content);

        }

        /// <summary>
        /// Runs every frame.
        /// </summary>
        /// <param name="gameTime">Instance of gameTime.</param>
        protected override void Update(GameTime gameTime)
        {
            // Pause the game, if it is not active.
            if (!IsActive && gameState != GameStates.Game)
            {
                return;
            }

            // Update the status of the keyboard.
            InputFactory.CheckState(gameTime);

            // Get Number of fps.
            fps = Fps.CheckFps(gameTime);                         

            // Set labels text.
            livesLabel.Text = "Lives: " + gameLives;
            scoreLabel.Text = "Score: " + gameScore;
            fpsLabel.Text = "FPS: " + fps;

            switch (gameState)
            {
                case GameStates.Game:
                {

                if ( !IsActive ) gameState = GameStates.Pause;
            

                 if ( InputFactory.EscPressed ) { gameState = GameStates.Pause; }

                    PlayerMoving();
                    BallMoving();

                  var brickName = _powerPackModel.Update(player.GetRectangle, bricks, gameTime);
                    if (brickName != String.Empty)
                    {
                      AddPoints(ref gameScore, brickName);
                      AddPoints( ref gameScore, brickName );
                    }

                    PlayerCollisionTest();
                    BrickCollisionTest();
                    break;
                }
                case GameStates.Idle:
                {
                    PlayerMoving();
                    // The ball follows the player.
                    ball.X = player.X + (PlayerWidth/2) - BallSize/2;
                    ball.Y = player.Y - BallSize;
                    // Handle keypress event beginning of the game
                    if (InputFactory.SpacePressed == true)
                    {
                    gameState = GameStates.Game;
                    }
                    break;
                }
                case GameStates.Menu:
                {
                    // Switch off sound
                    _soundFactory.Sound = false;
                    // Emulate user activity
                    AI();

                    BallMoving();


                    PlayerCollisionTest();
                    BrickCollisionTest();

                    MenuController();
                    break;
                }
                case GameStates.Pause:
                {

                    PauseController();
                    break;
                }

                case GameStates.Intro:
                {
                    if (InputFactory.EnterPressed) gameState = GameStates.Menu;

                    introBackground.FadeOpacity(gameTime, true);
                    break;
                }
            }


            base.Update(gameTime);
        }

        /// <summary>
        /// Draw method 
        /// </summary>
        /// <param name="gameTime">Gametime</param>
        protected override void Draw(GameTime gameTime)
        {
            // Fills the screen black.
            GraphicsDevice.Clear(Color.Black);
            // Begin draw.
            spriteBatch.Begin();

            switch (gameState)
            {
                case GameStates.Game:
                {
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();

                    _powerPackModel.Draw(spriteBatch);

                    scoreLabel.Draw(spriteBatch);
                    livesLabel.Draw(spriteBatch);
                    fpsLabel.Draw(spriteBatch);


                    break;
                }
                case GameStates.Idle:
                {
                    if (InputFactory.EscPressed) { gameState = GameStates.Pause; }
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();
                    enterGameLabel.Draw(spriteBatch);

                    scoreLabel.Draw(spriteBatch);
                    livesLabel.Draw(spriteBatch);
                    fpsLabel.Draw(spriteBatch);
                    break;
                }
                case GameStates.Menu:
                {
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();

                    menuBgRect.Draw(spriteBatch);
                    menuStartRect.Draw(spriteBatch);
                    menuAboutRect.Draw(spriteBatch);
                    menuExitRect.Draw(spriteBatch);
                    break;
                }
                case GameStates.Pause:
                {
                    player.Draw(spriteBatch);
                    ball.Draw(spriteBatch);
                    DrawBricks();

                    pauseBackgroundRectangle.Draw(spriteBatch);
                    pauseRectangle.Draw(spriteBatch);
                    pauseRectangleUpper.Draw(spriteBatch);

                    continueButtonRectangle.Draw(spriteBatch);
                    gotoMainButtonRectangle.Draw(spriteBatch);
                    exitButtonRectangle.Draw(spriteBatch);

                    continueButtonLabel.Draw(spriteBatch);
                    gotoMainButtonLabel.Draw(spriteBatch);
                    exitButtonLabel.Draw(spriteBatch);
                    pauseLabel.Draw(spriteBatch);
                    break;
                }
                case GameStates.Intro:
                {
                    introBackground.Draw(spriteBatch);
                    break;
                }
            }


            // End draw.
            spriteBatch.End();

            base.Draw(gameTime);
        }

        /// <summary>
        /// Draws the bricks from the array List.
        /// </summary>
        private void DrawBricks( )
            {
            for ( int i = 0; i < bricks.Count( ); i++ )
                if ( bricks[ i ] != null )
                    bricks[ i ].Draw( spriteBatch );
            }
    }
}
