﻿using Microsoft.Xna.Framework;

namespace BreakoutGame
{
    /// <summary>
    /// Collision detection
    /// </summary>
    public static class Collision
    {
        /// <summary>
        /// Check collision between two objects
        /// </summary>
        /// <param name="x1">X coordinate of the object 1</param>
        /// <param name="y1">Y coordinate of the object 1</param>
        /// <param name="width1">Object 1 width</param>
        /// <param name="height1">Object 1 height</param>
        /// <param name="x2">X coordinate of the object 2</param>
        /// <param name="y2">Y coordinate of the object 1</param>
        /// <param name="width2">Object 2 width</param>
        /// <param name="height2">Object 2 height</param>
        /// <returns>Collision Result</returns>
        public static bool IsTouching(int x1, int y1, int width1, int height1, int x2, int y2, int width2, int height2)
        {
            return (x1 - width2 <= x2) && (x1 + width1 >= x2) && (y1 - height2 <= y2) && (y1 + height1 >= y2);
        }


        /// <summary>
        /// Determines whether the specified rectangles is touching.
        /// </summary>
        /// <param name="r1">The first rectangle.</param>
        /// <param name="r2">The second rectangle.</param>
        /// <returns>Collision Result</returns>
        public static bool IsTouching(Rectangle r1, Rectangle r2)
        {
            return (r1.X - r2.Width <= r2.X) && (r1.X + r1.Width >= r2.X) && (r1.Y - r2.Height <= r2.Y) && (r1.Y + r1.Height >= r2.Y);
        }
    }

}
