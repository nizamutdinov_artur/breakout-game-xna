﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutGame.GameObjects
{
    internal class MyLabel
    {
        private readonly SpriteFont _font1;
        private Vector2 _fontPos;
        


        /// <summary>
        /// Label constructor
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="text">Label text</param>
        /// <param name="color">Color of the label</param>
        public MyLabel(ContentManager contentManager, int x, int y, string text, Color color)
        {
            Color = color;

            Text = text;

            _font1 = contentManager.Load<SpriteFont>("MyFont");

            _fontPos = new Vector2(x, y);
        }

        /// <summary>
        /// Gets or sets the X coordinate.
        /// </summary>
        /// <value>
        /// The X coordinate.
        /// </value>
        public int X
        {
            get { return (int) _fontPos.X; }
            set { _fontPos.X = value; }
        }

        /// <summary>
        /// Gets or sets the Y coordinate.
        /// </summary>
        /// <value>
        /// The Y coordinate.
        /// </value>
        public int Y
            {
            get { return ( int )_fontPos.Y; }
            set { _fontPos.Y = value; }
            }


        /// <summary>
        ///  Get or Set text of the label
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the color.
        /// </summary>
        /// <value>
        /// The color.
        /// </value>
        public Color Color { get; set; }

        /// <summary>
        /// Draw method
        /// </summary>
        /// <param name="spriteBatch">Instance of main SpriteBatch class</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(_font1, Text, _fontPos, Color);
        }
    }
}