﻿using Microsoft.Xna.Framework.Content;

namespace BreakoutGame.GameObjects
{

    /// <summary>
    /// Description of the ball object
    /// </summary>
    public class Ball : MyShape
    {

        /// <summary>
        /// Ball constructor
        /// </summary>
        /// <param name="contentManager">Instance of Content manager</param>
        /// <param name="spriteName">texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="radius">ball radius</param>
        public Ball(ContentManager contentManager, string spriteName, int x, int y, int radius) : base(contentManager, spriteName, x, y, radius, radius, 1.0f)
        {
        }

    }
}