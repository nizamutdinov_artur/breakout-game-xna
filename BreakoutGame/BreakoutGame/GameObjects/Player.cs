﻿using Microsoft.Xna.Framework.Content;

namespace BreakoutGame.GameObjects
{

    /// <summary>
    /// Description of the player object
    /// </summary>
    public class Player   : MyShape
    {
        private int _moveSpeed = 12;

        /// <summary>
        /// Player constructor
        /// </summary>
        /// <param name="contentManager">Instance of Content Manager</param>
        /// <param name="spriteName">Texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">Player width</param>
        /// <param name="height">Player heigth</param>
        public Player(ContentManager contentManager, string spriteName, int x, int y, int width, int height) : base (contentManager,spriteName,x,y,width,height,1.0f)
        {
        }

        /// <summary>
        /// Get or Set player MoveSpeed
        /// </summary>
        public int MoveSpeed
        {
            get { return _moveSpeed; }
            set { _moveSpeed = value; }
        }
    }
    }

      