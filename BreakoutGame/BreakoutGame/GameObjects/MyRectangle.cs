﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutGame.GameObjects
{

    /// <summary>
    /// Rectangle class.
    /// </summary>
    public class MyRectangle: MyShape
    {
    /// <summary>
    /// Time passed from last frame Update
    /// </summary>
        private float _elapsed;
        private float _frameRate= 0.1f;

        /// <summary>
        /// MyRectangle constructor with color filler.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="width">Width of the rectangle.</param>
        /// <param name="height">Height of the rectangle.</param>
        /// <param name="graphicsDevice">The device.</param>
        /// <param name="color">Color of the rectangle</param>
        /// <param name="opacity">Opacity of the rectangle.</param>
        public MyRectangle(float x, float y, int width, int height, GraphicsDevice graphicsDevice, Color color, float opacity)  : base(x,y,width,height,graphicsDevice,color,opacity)
        { 
        }

        /// <summary>
        /// MyRectangle constructor with image texture.
        /// </summary>
        /// <param name="x">X coordinate.</param>
        /// <param name="y">Y coordinate.</param>
        /// <param name="width">Width of the rectangle.</param>
        /// <param name="height">Height of the rectangle.</param>
        /// <param name="contentManager">Instance of Content Manager.</param>
        /// <param name="spriteName">Texture name.</param>
        /// <param name="opacity"></param>
        public MyRectangle( ContentManager contentManager, string spriteName, float x, float y, int width, int height, float opacity ) :base (contentManager,spriteName,x,y,width,height,opacity)
        {
        }

        /// <summary>
        /// Fades the opacity.
        /// </summary>
        /// <param name="gametime">The gametime.</param>
        /// <param name="opacityUp">if set to <c>true</c> [opacity up].</param>
        public void FadeOpacity(GameTime gametime, bool opacityUp)
        {
            switch (opacityUp)
            {
                case true:
                {
                    _elapsed += (float) gametime.ElapsedGameTime.TotalSeconds;

                    // Until it passes our frame length
                    if (_elapsed > _frameRate)
                    {
                        if (_opacity <= 1.0f)
                        {
                            _opacity += 0.05f;
                        }

                        // Reset the elapsed frame time.
                        _elapsed = 0.0f;

                    }
                    break;
                }
                
                case false:
                {
                    _elapsed += (float) gametime.ElapsedGameTime.TotalSeconds;

                    // Until it passes our frame length
                    if (_elapsed > _frameRate)
                    {
                         if (_opacity <= 0.0f)
                        {
                            _opacity -= 0.001f;
                        }

                        // Reset the elapsed frame time.
                        _elapsed = 0.0f;

                    }
                    break;
                }

            }

        }

    }
}