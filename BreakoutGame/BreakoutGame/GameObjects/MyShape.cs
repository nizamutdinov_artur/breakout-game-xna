﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutGame.GameObjects
{

    /// <summary>
    /// Class includes a basic package requered for rectangular object
    /// </summary>
    public class MyShape
    {

        #region Fields

        /// <summary>
        /// The rectangle.
        /// </summary>
        protected Rectangle _rectangle;

        /// <summary>
        /// The sprite.
        /// </summary>
        protected Texture2D _sprite;

        /// <summary>
        /// The opacity.
        /// </summary>
        protected float _opacity;

        #endregion

        #region Constructors

        /// <summary>
        /// Rectangular object with texture fill.
        /// </summary>
        /// <param name="contentManager">Instance of the content manager class.</param>
        /// <param name="spriteName">Sprite name of the object.</param>
        /// <param name="x">X coordinate of the object.</param>
        /// <param name="y">Y coordinate of the object.</param>
        /// <param name="width">Width of the object.</param>
        /// <param name="height">Height of the object.</param>
        /// <param name="opacity">Opacity of the object.</param>
        public MyShape(ContentManager contentManager, string spriteName, float x, float y, int width, int height, float opacity)
        {


            _rectangle.Width = width;
            _rectangle.Height = height;

            _opacity = opacity;

            // load _sprite, create rectangle
            _sprite = contentManager.Load<Texture2D>(spriteName);
            _rectangle = new Rectangle((int) x, (int) y, width, height);
        }


        /// <summary>
        /// Rectangular object with color fill.
        /// </summary>
        /// <param name="x">X coordinate of the object.</param>
        /// <param name="y">Y coordinate of the object.</param>
        /// <param name="width">Width of the object.</param>
        /// <param name="height">Height of the object.</param>
        /// <param name="graphicsDevice">Instance of the graphic device class.</param>
        /// <param name="color">Color of the object.</param>
        /// <param name="opacity">Opacity of the object.</param>
        public MyShape(float x, float y, int width, int height, GraphicsDevice graphicsDevice, Color color, float opacity)
        {
            _rectangle.Width = width;
            _rectangle.Height = height;

            _opacity = opacity;

            var data = new Color[width*height];

            for (int i = 0; i < data.Length; i++)
            {
                data[i] = color;
            }

            _sprite = new Texture2D(graphicsDevice, width, height);
            _sprite.SetData(data);

            _rectangle = new Rectangle((int) x, (int) y, width, height);

        }

        #endregion

        #region Methods

        /// <summary>
        /// Draw method.
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite, _rectangle, Color.White*_opacity);
        }

        #endregion

        #region Properties



        /// <summary>
        /// Gets or sets the X coordinate.
        /// </summary>
        /// <value>
        /// The X coordinate.
        /// </value>
        public int X
        {
            get { return _rectangle.X; }
            set { _rectangle.X = value; }
        }

        /// <summary>
        /// Gets or sets the Y coordinate.
        /// </summary>
        /// <value>
        /// The Y coordinate.
        /// </value>
        public int Y
        {
            get { return _rectangle.Y; }
            set { _rectangle.Y = value; }
        }

        /// <summary>
        /// Gets or sets the width.
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public int Width
        {
            get { return _rectangle.Width; }
            set { _rectangle.Width = value; }
        }

        /// <summary>
        /// Gets or sets the heigth.
        /// </summary>
        /// <value>
        /// The heigth.
        /// </value>
        public int Height
        {
            get { return _rectangle.Height; }
            set { _rectangle.Height = value; }
        }


        /// <summary>
        /// Gets or sets the opacity.
        /// </summary>
        /// <value>
        /// The opacity.
        /// </value>
        public float Opacity
        {
            get { return _opacity; }
            set { _opacity = value; }
        }


        /// <summary>
        /// Gets the rectangle.
        /// </summary>
        /// <value>
        /// The rectangle.
        /// </value>
        public Rectangle GetRectangle
        {
            get { return _rectangle; }
        }


        /// <summary>
        /// Gets or sets the sprite.
        /// </summary>
        /// <value>
        /// The sprite.
        /// </value>
        public Texture2D Sprite
        {
            get { return _sprite; }
            set { _sprite = value; }
        }


        #endregion

    }
}