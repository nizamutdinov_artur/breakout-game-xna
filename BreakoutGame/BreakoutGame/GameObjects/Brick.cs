﻿using Microsoft.Xna.Framework.Content;

namespace BreakoutGame.GameObjects
{
    /// <summary>
    /// Description of the Brick object
    /// </summary>
    public class Brick : MyShape
    {

        private readonly string _spriteName;

        /// <summary>
        /// For placing bricks carefully, constructor takes the position and some parameters.
        /// TODO: brick lives 
        /// TODO: Crashed field
        /// </summary>
        /// <param name="contentManager">instance of Content Manager</param>
        /// <param name="spriteName">texture name</param>
        /// <param name="x">X coordinate</param>
        /// <param name="y">Y coordinate</param>
        /// <param name="width">brick width</param>
        /// <param name="height">brick heigth</param>
        public Brick(ContentManager contentManager, string spriteName, int x, int y, int width, int height) : base(contentManager, spriteName, x, y, width, height, 1.0f)
        {
            _spriteName = spriteName;

        }

        /// <summary>
        /// Get name of sprite
        /// </summary>
        public string SpriteName
        {
            get { return _spriteName; }
        }

    }
}
