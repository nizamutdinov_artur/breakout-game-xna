﻿using Microsoft.Xna.Framework.Content;

namespace BreakoutGame.GameObjects
{

    /// <summary>
    /// Power pack class.
    /// </summary>
    public class PowerPack : MyShape
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerPack"/> class.
        /// </summary>
        /// <param name="contentManager">Instance of the content manager class.</param>
        /// <param name="spriteName">Sprite name of the object.</param>
        /// <param name="x">X coordinate of the object.</param>
        /// <param name="y">Y coordinate of the object.</param>
        /// <param name="width">Width of the object.</param>
        /// <param name="height">Height of the object.</param>
        /// <param name="opacity">Opacity of the object.</param>
        public PowerPack(ContentManager contentManager, string spriteName, float x, float y, int width, int height, float opacity) : base(contentManager, spriteName, x, y, width, height, opacity)
        {
            Activated = false;
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="PowerPack"/> is activated.
        /// </summary>
        /// <value>
        ///   <c>true</c> if activated; otherwise, <c>false</c>.
        /// </value>
        public bool Activated { get; set; }
    }
}