﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutGame.GameObjects
{

    /// <summary>
    /// Animated Sprite class.
    /// </summary>
    public class AnimatedSprite : MyShape
    {

        private double _fps = 30;
        private readonly int _frameCount;
        private int _currentFrame;

        private float _elapsed;

        private readonly int _frameOffsetX;

        private readonly int _frameOffsetY;

        private bool _animating = true;

        /// <summary>
        /// Initializes a new instance of the <see cref="AnimatedSprite"/> class.
        /// </summary>
        /// <param name="frameCount">The frame count.</param>
        /// <param name="frameOffsetX">The frame offset x.</param>
        /// <param name="frameOffsetY">The frame offset y.</param>
        /// <param name="content">The content.</param>
        /// <param name="spriteName">Name of the sprite.</param>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="width">The width.</param>
        /// <param name="heigth">The heigth.</param>
        /// <param name="opacity">The opacity.</param>
        public AnimatedSprite( int frameCount,int frameOffsetX, int frameOffsetY, ContentManager content, string spriteName, float x, float y, int width, int heigth,float opacity) : base(content, spriteName, x, y, width, heigth, opacity)
        {
            _frameOffsetX = frameOffsetX;
            _frameOffsetY = frameOffsetY;
            _frameCount = frameCount;
            _currentFrame = 0;
        }


        /// <summary>
        /// Updates the specified game time.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        /// <param name="loop">if set to <c>true</c> [loop].</param>
        public void Update(GameTime gameTime, bool loop)
        {
            if (loop)
            {
                _elapsed += gameTime.ElapsedGameTime.Milliseconds;
                if (_elapsed > (1000/_fps))
                {
                    if (_animating)
                    {
                        if (_currentFrame == _frameCount)
                        {
                            _currentFrame = 0;
                        }
                        _currentFrame++;
                    }
                    _elapsed = 0.0f;
                }

            } else
            {
                _elapsed += gameTime.ElapsedGameTime.Milliseconds;
                if (_elapsed > (1000/_fps))
                {
                    if (_animating)
                    {
                        if (_currentFrame == _frameCount)
                        {
                            _animating = false;
                        }
                        _currentFrame++;
                    }
                    _elapsed = 0.0f;
                }
            }

        }


        /// <summary>
        /// Gets or sets the current frame.
        /// </summary>
        /// <value>
        /// Current frame.
        /// </value>
        public int Frame
        {
            get { return _currentFrame; }
            set { _currentFrame = (int) MathHelper.Clamp(value, 0, _frameCount); }
        }

        /// <summary>
        /// Gets or sets the FPS.
        /// </summary>
        /// <value>
        /// The FPS.
        /// </value>
        public double Fps
            {
            get { return _fps; }
            set { _fps = (float) Math.Max(value, 0f); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this instance is animating.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is animating; otherwise, <c>false</c>.
        /// </value>
        public bool IsAnimating
        {
            get { return _animating; }
            set { _animating = value; }
        }

        /// <summary>
        /// Gets the source rectangle.
        /// </summary>
        /// <returns>The current frame source rectangle.</returns>
        public Rectangle GetSourceRectangle()
        {
            return new Rectangle(_frameOffsetX + (Width*_currentFrame), _frameOffsetY, Width, Height);
        }


        /// <summary>
        /// Draw method.
        /// </summary>
        /// <param name="spriteBatch">Instance of SpriteBatch class.</param>
        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(_sprite,new Vector2(X,Y), GetSourceRectangle(), Color.White*_opacity);
        }

    }
}
