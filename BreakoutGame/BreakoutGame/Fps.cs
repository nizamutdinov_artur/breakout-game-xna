﻿
namespace BreakoutGame
{
    internal static class Fps
    {
        private static float elapsedTime;
        private static int totalFrames;
        private static int fps = 0;

        /// <summary>
        /// Get fps if second passed
        /// </summary>
        /// <param name="gameTime">Instance of XNA gameTime class</param>
        public static int CheckFps(Microsoft.Xna.Framework.GameTime gameTime)
        {

            elapsedTime += (float) gameTime.ElapsedGameTime.TotalMilliseconds;
            totalFrames++;

            // 1 Second has passed
            if (elapsedTime >= 1000.0f)
            {
                fps = totalFrames;
                totalFrames = 0;
                elapsedTime = 0;
            }
            return fps;
        }


    }
}
