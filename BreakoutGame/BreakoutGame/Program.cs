﻿
namespace BreakoutGame
    {
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (MyGame myGame = new MyGame())
            {
                myGame.Run();
            }
        }
    }
#endif
    }

