﻿using System;
using System.Collections.Generic;
using BreakoutGame.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace BreakoutGame
{

    /// <summary>
    /// Power pack class
    /// </summary>
    public class PowerPackManager
    {

        private List<PowerPack> _powerPacksList;

        private readonly int _width;
        private readonly int _height;

        private ContentManager _content;
        private Random _random;

        private AnimatedSprite _explosion;

        /// <summary>
        /// Gets or sets the move speed.
        /// </summary>
        /// <value>
        /// The move speed.
        /// </value>
        public int MoveSpeed { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PowerPackManager"/> class.
        /// </summary>
        public PowerPackManager(ContentManager content)
        {
            MoveSpeed = 5;
            _width = 20;
            _height = 25;
            _content = content;
            _random = new Random( );
            _powerPacksList = new List<PowerPack>();
        }

        /// <summary>
        /// Update method.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="bricks">The bricks.</param>
        /// <param name="gameTime">The game time.</param>
        /// <returns></returns>
        public string Update(Rectangle player, List<Brick> bricks, GameTime gameTime)
        {
            if (_explosion != null)
      _explosion.Update(gameTime,false);

            return Move(player, bricks);

        }

        private string Move(Rectangle player, List<Brick> bricks)
        {
            for (int i = 0; i < _powerPacksList.Count; i++)
            {
                if (_powerPacksList[i].Activated)
                {
                    if (_powerPacksList[i].Y > 0)
                    {
                        if (_powerPacksList[i].Height == _height + 20)
                        {
                            _powerPacksList[i].Height += 20;
                            _powerPacksList[i].Width += 20;
                        }
                        _powerPacksList[i].Y -= MoveSpeed;

                        for (int j = 0; j < bricks.Count; j++)
                        {
                            if (bricks[j] == null) continue;

                            if (Collision.IsTouching(bricks[j].GetRectangle, _powerPacksList[i].GetRectangle))
                            {
                               
                               _explosion = null;
                               _explosion = new AnimatedSprite( 16, 0, _random.Next( 0, 5 ) * MyGame.ExplosionSize, _content, "GameObject\\explosions", bricks[ j ].X, bricks[ j ].Y, MyGame.ExplosionSize, MyGame.ExplosionSize, 1.0f );


                               var foo = bricks[ j ].SpriteName;
                               bricks[ j ] = null;
                                return foo;
                            }
                        }
                    }
                    else
                    {
                        _powerPacksList.RemoveAt(i);

                    }

                }
                else
                {
                    if (Collision.IsTouching(player, _powerPacksList[i].GetRectangle))
                    {
                        _powerPacksList[i].Activated = true;
                        continue;
                    }

                    if (_powerPacksList[i].Y < MyGame.WindowHeight)
                    {
                        _powerPacksList[i].Y += MoveSpeed;
                    }
                    else
                    {
                        _powerPacksList.RemoveAt(i);
                    }
                }
            }
            return String.Empty;
        }




        /// <summary>
        /// Adds the entity.
        /// </summary>
        /// <param name="content">The content.</param>
        /// <param name="pos">The position.</param>
        /// <param name="type">The type.</param>
        public void AddEntity(ContentManager content, Vector2 pos, String type)
        {
            _powerPacksList.Add(new PowerPack(content, "GameObject\\penguin", pos.X, pos.Y, _width, _height, 1.0f));
        }

        /// <summary>
        /// Cleans up.
        /// </summary>
        public void CleanUp()
        {
            for (int i = 0; i < _powerPacksList.Count; i++)
            {
                _powerPacksList.RemoveAt(i);
            }

            _powerPacksList = new List<PowerPack>();
            _explosion = null;

        }

        /// <summary>
        /// Draw method.
        /// </summary>
        /// <param name="spriteBatch">The sprite batch.</param>
        public void Draw(SpriteBatch spriteBatch)
        {
            foreach (PowerPack pack in _powerPacksList)
            {
                pack.Draw(spriteBatch);
            }
            if (_explosion != null)
                _explosion.Draw(spriteBatch);

        }
    }
}