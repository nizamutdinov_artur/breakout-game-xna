﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;

namespace BreakoutGame
{

    /// <summary>
    /// Class contains the sound effects
    /// </summary>
    public class SoundFactory
    {

        /// <summary>
        /// The sound
        /// </summary>
        public bool Sound = false;

        private readonly SoundEffect _playerSoundEffect;
        private readonly SoundEffect _brickSoundEffect;
        private readonly SoundEffect _wallSoundEffect;
        private readonly SoundEffect _ballMissedSoundEffect;


        /// <summary>
        /// Initializes a new instance of the <see cref="SoundFactory"/> class.
        /// </summary>
        /// <param name="content">The content.</param>
        public SoundFactory(ContentManager content)
        {
            _playerSoundEffect = content.Load<SoundEffect>("Sound\\paddleSound");
            _brickSoundEffect = content.Load<SoundEffect>( "Sound\\brickSound" );
            _wallSoundEffect = content.Load<SoundEffect>( "Sound\\wallSound" );
            _ballMissedSoundEffect = content.Load<SoundEffect>( "Sound\\ballMissedSound" );
        }


        /// <summary>
        /// Plays the specified Sound Effect
        /// </summary>
        /// <param name="FileName">Name of the file.</param>
        public void Play(string FileName)
        {
            if (Sound)
            {
                switch (FileName)
                {
                    case "player":
                        _playerSoundEffect.Play();
                        break;
                    case "brick":
                        _brickSoundEffect.Play();
                        break;
                    case "wall":
                        _wallSoundEffect.Play();
                        break;
                    case "ballMissed":
                        _ballMissedSoundEffect.Play();
                        break;
                }
            }
        }
    }
}