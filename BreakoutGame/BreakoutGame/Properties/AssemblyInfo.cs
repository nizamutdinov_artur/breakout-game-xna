﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle( "BreakoutGame" )]
[assembly: AssemblyProduct( "BreakoutGame" )]
[assembly: AssemblyDescription( "Однопользовательская аркадная игра" )]
[assembly: AssemblyCompany( "БГПУ" )]
[assembly: AssemblyCopyright( "Copyright ©  2015" )]
[assembly: AssemblyTrademark( "" )]
[assembly: AssemblyCulture( "" )]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type. Only Windows
// assemblies support COM.
[assembly: ComVisible( true )]

// On Windows, the following GUID is for the ID of the typelib if this
// project is exposed to COM. On other platforms, it unique identifies the
// title storage container when deploying this assembly to the device.
[assembly: Guid( "7e3ad7c7-ef7d-47bd-94f7-7b0be0c6ad1a" )]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion( "1.0.0.0" )]
[assembly: NeutralResourcesLanguageAttribute( "en" )]
