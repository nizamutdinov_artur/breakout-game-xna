﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using BreakoutGame.GameObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BreakoutGame
{
    /// <summary>
    /// Second part of the game class.
    /// Methods.
    /// </summary>
    public partial class MyGame : Microsoft.Xna.Framework.Game
    {
        /// <summary>
        /// TODO: MENU BUTTONS
        /// </summary>
        private void MenuController()
        {
                                          
            menuStartRect.Opacity = 0.0f;
            menuAboutRect.Opacity = 0.0f;
            menuExitRect.Opacity = 0.0f;

            if (Collision.IsTouching(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, menuStartRect.X, menuStartRect.Y, menuStartRect.Width, menuStartRect.Height))
            {
                menuStartRect.Opacity = 0.5f;

                if (InputFactory.LMousePressed)
                {
                    _soundFactory.Sound = true;
                    gameState = GameStates.Idle;
                    ResetGame();

                }
            }


            if (Collision.IsTouching(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, menuAboutRect.X, menuAboutRect.Y, menuAboutRect.Width, menuAboutRect.Height))
            {
                menuAboutRect.Opacity = 0.5f;

                if (InputFactory.LMousePressed)
                {
                    // TODO:  
                }
            }

            if (Collision.IsTouching(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, menuExitRect.X, menuExitRect.Y, menuExitRect.Width, menuExitRect.Height))
            {
                menuExitRect.Opacity = 0.5f;

                if (InputFactory.LMousePressed)
                {
                    if (System.Windows.Forms.MessageBox.Show("Really ?", "Game", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                    {
                        Exit();
                    }
                }

            }
        }

        /// <summary>
        /// TODO:
        /// </summary>
        private void PauseController()
        {
            if (InputFactory.LMousePressed && Collision.IsTouching(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, continueButtonRectangle.X, continueButtonRectangle.Y, continueButtonRectangle.Width, continueButtonRectangle.Height))
            {
                gameState = GameStates.Game;

            }

            if (InputFactory.LMousePressed && Collision.IsTouching(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, gotoMainButtonRectangle.X, gotoMainButtonRectangle.Y, gotoMainButtonRectangle.Width, gotoMainButtonRectangle.Height))
            {
                gameState = GameStates.Menu;

            }

            if (InputFactory.LMousePressed && Collision.IsTouching(Mouse.GetState().X, Mouse.GetState().Y, 1, 1, exitButtonRectangle.X, exitButtonRectangle.Y, exitButtonRectangle.Width, exitButtonRectangle.Height))
            {
                if (System.Windows.Forms.MessageBox.Show("Really ?", "Game", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    Exit();
                }
            }
        }

        /// <summary>
        /// TODO: AI.
        /// </summary>
        private void AI()
        {

            if (ball.Y < WindowHeight - 40)
            {
                player.X = (ball.X) - (PlayerWidth/2);
            }
        }

  

        /// <summary>
        /// Checks collision bricks with the ball.
        /// </summary>
        private void BrickCollisionTest()
        {
            for (int i = 0; i < bricks.Count(); i++)
            {
                if (bricks[i] != null && Collision.IsTouching(bricks[i].GetRectangle, ball.GetRectangle))
                {
                    // Plays sound
                    _soundFactory.Play("brick");
                    // Add one
                    ballToBrickCollisionCounter++;

                    // Find the angle between center of brick and a ball center.
                    int angle = (int) (Math.Atan2((bricks[i].Y + BrickHeight/2) - (ball.Y + BallSize/2),
                        (bricks[i].X + BrickWidth/2) - (ball.X + BallSize/2))/Math.PI*180);

                    if (angle < 10 && angle > -10) // Angles between -10 and 10.
                        ballVelocityX = -Math.Abs(ballVelocityX);
                    else if ((angle < 180 && angle > 170) || (angle > -180 && angle < -170)) // Angles between 180 and -170 
                        ballVelocityX = Math.Abs(ballVelocityX);
                    else if (angle > 10 && angle < 170)
                        BallVelocityY = -Math.Abs(BallVelocityY);
                    else if (angle < -10 && angle > -170)
                        BallVelocityY = Math.Abs(BallVelocityY);

                   AddPoints(ref gameScore, bricks[i].SpriteName);

                    if (gameState == GameStates.Game)
                    {
                       // explosion = null;
                       // explosion = new AnimatedSprite(16, 0, random.Next(0, 5)*ExplosionSize, Content, "explosions", ball.X, ball.Y, ExplosionSize, ExplosionSize, 1.0f);

                        if (random.Next(1, 10) == 1)
                        {
                            _powerPackModel.AddEntity(Content, new Vector2(bricks[i].X+ BrickWidth/2, bricks[i].Y), "NULL");

                        }
                    }
                    // After 4 touches speed++
                    if (ballToBrickCollisionCounter >= 4)
                    {
                        ballSpeedMultiplier += random.Next(1, 10);
                        ballToBrickCollisionCounter = 0;
                    }
                    if (bricks[i].SpriteName == GameBrickRed)
                    {
                        ballSpeedMultiplier += random.Next( 1, 10 ); // Set random speed multiplier
                    }
                    bricks[i] = null;
                    break;
                }
            }
        }

        private void AddPoints(ref int points, string spriteName)
        {
        // Different sprites gets a different amount of score
        switch ( spriteName)
            {
            case MyGame.GameBrickRed:
            points += GameBrickRedPoints;
            break;
            case MyGame.GameBrickOrange1:
           points += GameBrickOrangePoints;
            break;
            case MyGame.GameBrickOrange2:
            points += GameBrickOrangePoints;
            break;
            case MyGame.GameBrickYellow:
            points += GameBrickYellowPoints;
            break;
            case GameBrickGreen:
            points += GameBrickGreenPoints;
            break;
            case MyGame.GameBrickBlue:
            points += GameBrickBluePoints;
            break;
            default:
            points += 0;
            break;
            }
        }

        /// <summary>
        /// Players moving.
        /// Fixed 14.05.
        /// </summary>
        public void PlayerMoving( )
            {
            if ( InputFactory.DPressed && ( player.X < MyGame.WindowWidth - player.Width ) )
                {
                player.X += player.MoveSpeed;
                }

            if ( InputFactory.APressed && ( player.X > 0 ) )
                {
                player.X -= player.MoveSpeed;
                }

            if ( InputFactory.RightPressed && ( player.X < MyGame.WindowWidth - player.Width ) )
                {
                player.X += player.MoveSpeed;
                }

            if ( InputFactory.LeftPressed && ( player.X > 0 ) )
                {
                player.X -= player.MoveSpeed;
                }
            }

        /// <summary>
        /// Checks collision player with ball.
        /// </summary>
        private void PlayerCollisionTest()
        {
            if (Collision.IsTouching(player.GetRectangle,ball.GetRectangle))
            {
                _soundFactory.Play("player");

                double difference = (ball.X + BallSize/2) - (player.X + PlayerWidth/2);
                ballVelocityX = (difference/Math.Abs(difference))*0.9 + difference/(PlayerWidth); 
                BallVelocityY = -Math.Sqrt(2.5 - ballVelocityX*ballVelocityX);
            }

        }

 

        /// <summary>
        /// This method include ball moving
        /// and the Wall collision tester.
        /// </summary>
        public void BallMoving()
        {
            ball.X += (int) (ballVelocityX*(ballSpeedMultiplier/10));
            ball.Y += (int) (BallVelocityY*(ballSpeedMultiplier/10));

            ballSpeedMultiplier -= 0.01f;

            // Bottom
            if (ball.Y > WindowHeight - BallSize)
            {
                BallMissed();
                return;
            }

            // Left
            if (ball.X < 0)
            {
                ballVelocityX = Math.Abs(ballVelocityX);
                _soundFactory.Play("wall");
            }
            // Right
            if (ball.X > WindowWidth - BallSize)
            {
                ballVelocityX = -Math.Abs(ballVelocityX);
                _soundFactory.Play( "wall" );
            }
            // Upper
            if (ball.Y < 0)
            {
                BallVelocityY = Math.Abs(BallVelocityY);
                _soundFactory.Play( "wall" );
            }


        }

        /// <summary>
        /// What happends if player miss the ball.
        /// </summary>
        private void BallMissed()
        {

            _soundFactory.Play("ballMissed");

            if (gameLives > 1)
            {
                // Take live.
                gameLives--;
                // Set speed to default.
                ballSpeedMultiplier = 60;
                // Set a game state to Idle.
                gameState = GameStates.Idle;
                _powerPackModel.CleanUp();
            } else
            {
                // Displays a dialog window to continue the game or exit.
                if (System.Windows.Forms.MessageBox.Show("Вы проиграли.\n Ваш счет: " + gameScore + " Очков", "Game", System.Windows.Forms.MessageBoxButtons.RetryCancel, System.Windows.Forms.MessageBoxIcon.Asterisk) == System.Windows.Forms.DialogResult.Retry)
                {
                    ResetGame();
                } else
                {
                    Exit();
                }
            }
        }

        /// <summary>
        /// Resets the game.
        /// </summary>
        private void ResetGame()
        {
            ballSpeedMultiplier = 60;
            gameState = GameStates.Idle;
            gameScore = 0;
            gameLives = 3;
            // Set the position of the player in the center of the screen.
            player.X = (WindowWidth/2) - (PlayerWidth/2);
            // Clear all the bricks from the stage.
            bricks = new List<Brick>();
            _powerPackModel.CleanUp();

            InitBricks();

        }

        /// <summary>
        /// Initializes the bricks.
        /// </summary>
        private void InitBricks()
        {
            for (int i = 0; i < BrickColumn; i++)
                for (int j = 0; j < BrickRow; j++)
                {
                    const int marginX = 0;
                    const int marginY = 50;

                    int distanceX = j*80;
                    int distanceY = i*15;

                    // Different action for the specified column
                    switch (i)
                    {
                        case 0:
                        {
                            bricks.Add(new Brick(Content, GameBrickRed, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                        case 1:
                        {
                            bricks.Add(new Brick(Content, GameBrickOrange1, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                        case 2:
                        {
                            bricks.Add(new Brick(Content, GameBrickOrange2, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                        case 3:
                        {
                            bricks.Add(new Brick(Content, GameBrickYellow, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                        case 4:
                        {
                            bricks.Add(new Brick(Content, GameBrickGreen, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                        case 5:
                        {
                            bricks.Add(new Brick(Content, GameBrickBlue, marginX + distanceX, marginY + distanceY, BrickWidth, BrickHeight));
                            break;
                        }
                    }
                }

        }

    }
}
