﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace BreakoutGame
{
    /// <summary>
    /// TODO: Write comments
    /// </summary>
    public static class InputFactory
    {
        
        private static bool _aPressed = false;
        private static bool _dPressed = false;
        private static bool _leftPressed = false;
        private static bool _rightPressed = false;

        private static bool _enterPressed = false;
        private static bool _spacePressed = false;
        private static bool _escPressed = false;
        private static bool _lMousePressed = false;

        #region Public


        /// <summary>
        /// Gets a value indicating whether [a pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [a pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool APressed
        {
            get { return _aPressed; }
            private set { _aPressed = value; }
        }

        /// <summary>
        /// Gets a value indicating whether [d pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [d pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool DPressed
        {
            get { return _dPressed; }
            private set { _dPressed = value; }
        }

        /// <summary>
        /// Gets a value indicating whether [left pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [left pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool LeftPressed
        {
            get { return _leftPressed; }
            private set { _leftPressed = value; }
        }

        /// <summary>
        /// Gets a value indicating whether [right pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [right pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool RightPressed
        {
            get { return _rightPressed; }
            private set { _rightPressed = value; }
        }

        /// <summary>
        /// Gets a value indicating whether [enter pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [enter pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool EnterPressed
        {
            get { return _enterPressed; }
            private set { _enterPressed = value; }
        }

        /// <summary>
        /// Gets a value indicating whether [space pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [space pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool SpacePressed
        {
            get { return _spacePressed; }
            private set { _spacePressed = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [l mouse pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [l mouse pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool LMousePressed
        {
            get { return _lMousePressed; }
            set { _lMousePressed = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [escape pressed].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [escape pressed]; otherwise, <c>false</c>.
        /// </value>
        public static bool EscPressed
        {
            get { return _escPressed; }
            set { _escPressed = value; }
        }

        #endregion

        /// <summary>
        /// Checks the button state.
        /// </summary>
        /// <param name="gameTime">The game time.</param>
        public static void CheckState(GameTime gameTime)
        {
        KeyboardState keybState = Keyboard.GetState( );

            _leftPressed = keybState.IsKeyDown(Keys.Left);
            _rightPressed = keybState.IsKeyDown(Keys.Right);
            _aPressed = keybState.IsKeyDown( Keys.A );
            _dPressed = keybState.IsKeyDown( Keys.D );

            _enterPressed = keybState.IsKeyDown( Keys.Enter );
            _spacePressed = keybState.IsKeyDown( Keys.Space );
            _escPressed = keybState.IsKeyDown( Keys.Escape );

            _lMousePressed = Mouse.GetState().LeftButton == ButtonState.Pressed;

        }
    }
}