﻿namespace BreakoutGame
    {

    /// <summary>
    /// Game states.
    /// </summary>
   public enum GameStates
        {

        /// <summary>
        /// The game
        /// </summary>
        Game,

        /// <summary>
        /// The idle
        /// </summary>
        Idle,

        /// <summary>
        /// The menu
        /// </summary>
        Menu,

        /// <summary>
        /// The pause
        /// </summary>
        Pause,

        /// <summary>
        /// The intro
        /// </summary>
        Intro
        }
    }
